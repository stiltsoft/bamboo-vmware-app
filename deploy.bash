#!/bin/bash -ex

JAR_PATH=`find target -name "bamboo-vmware-plugin-*.jar"`
#echo $JAR_PATH
JAR_FNAME=`basename $JAR_PATH`
#echo $JAR_FNAME
curl -X POST "https://${BB_AUTH_STRING}@api.bitbucket.org/2.0/repositories/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/downloads" --form files=@"target/$JAR_FNAME"