<div>
    <div id="aui-message-bar"></div>

    <table id="vms" class="aui" style="display: none;">
        <thead>
            <tr>
                <th>[@ww.text name='vmware.table.th.vm'/]</th>
                <th>[@ww.text name='vmware.table.th.processor'/]</th>
                <th>[@ww.text name='vmware.table.th.memory'/]</th>
                <th id="snapshot-header" colspan="2" width="55%">[@ww.text name='vmware.table.th.snapshots'/]</th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>

    <div class="buttons">
        <a id="available-vm-btn" class="cancel">[@ww.text name='vmware.btn.get-available-vms'/]</a>
    </div>
</div>

<script type="text/javascript">
    (function($) {

        window.VMware = {
            init: function() {
                var t = this;
                $('#available-vm-btn').click(function(e) {
                    clearMessageBar();
                    AJS.messages.info({body: "<p>Processing request...</p>"});
                    $.ajax({
                        type: 'POST',
                        url: BAMBOO.contextPath + '/plugins/vmware/getAvailableVMs.action',
                        data: t.getVMHostParams(),
                        dataType: 'json',
                        success: t.handleSuccessResult,
                        error: t.handleErrorResult
                    });
                    e.preventDefault();
                    return false;
                });
            },

            getVMHostParams: function () {
                var data = [];

                data.push("host=" + $('input[name="vmware.host"]').val());
                data.push("&login=" + $('input[name="vmware.login"]').val());
                data.push("&password=" + $('input[name="vmware.password"]').val());

                return data.join("");
            },

            handleSuccessResult: function(data) {
                if (data.error) {
                    VMware.handleErrorResult("", "", data.error);
                    return;
                }

                var tbody = $('<tbody />');
                $.each(data.vms, function(i, v) {
                    var o = $.parseJSON(v);
                    var nameLink = $('<a href="#"/>').text(o.name).click(function(e) {
                        $('input[name="vmware.vm"]').val(o.name);
                        if (isStartTask()) {
                            $('input:radio[id="vmware_start_vm_stateCURRENT"]').attr('checked', true);
                            $('input[name="vmware.vm.snapshot.name"]').val("");
                            setSnapshotInputState();
                        }
                        e.preventDefault();
                        return false;
                    });
                    var tr = $('<tr style="background-color: #ffffcc;"/>')
                            .append($('<td />').append(nameLink))
                            .append($('<td />').html(o.cpuCount))
                            .append($('<td />').html(o.memorySizeMb))
                            .append(getSnapshotTd(o.name, o.snapshotNames, true))
                            .append(getSnapshotTd(o.name, o.snapshotNames, false));
                    tbody.append(tr);
                });
                $('#vms tbody').replaceWith(tbody);
                initSnapshotColumn();
                $('#vms').show();

                var auiMessageBar = $('#aui-message-bar');
                auiMessageBar.html("");
                AJS.messages.success({body: "<p>Available virtual machines are listed in the table below</p>"});
                auiMessageBar.delay(2000).slideUp(300);
            },

            handleErrorResult: function(xhr, status, errorThrown) {
                $('#aui-message-bar').html("");
                AJS.messages.error({body: "<p>Error occurred: " + errorThrown + "</p>"});
            }
        };

        AJS.toInit(function() {
            VMware.init();
        });

    })(AJS.$);

    function initSnapshotColumn() {
        if (!isStartTask()) {
            AJS.$('#snapshot-header').hide();
            AJS.$('td.snapshot-column').hide();
        }
    }

    function getSnapshotTd(vmName, snapNameList, isFirstColumn) {
        var uList = AJS.$('<ul style="margin-top: 0;"/>');
        var snapTd = AJS.$('<td class="snapshot-column" style="padding-left: 0;"/>').append(uList);
        var listHalf = snapNameList.length / 2 + snapNameList.length % 2;
        AJS.$.each(snapNameList, function(index, value) {
            var snapLink = AJS.$('<a href="#"/>')
                    .text(value)
                    .click(function(e) {
                        AJS.$('input[name="vmware.vm"]').val(vmName);
                        AJS.$('input:radio[id="vmware_start_vm_stateSNAPSHOT"]').attr('checked', true);
                        setSnapshotInputState();
                        AJS.$('input[name="vmware.vm.snapshot.name"]').val(AJS.$(this).text());
                        e.preventDefault();
                        return false;
                    });
            var snapLinkItem = AJS.$('<li style="margin-top: 0;"/>').append(snapLink);

            var beforeHalf = index + 1 <= listHalf;
            if ((isFirstColumn && beforeHalf) || (!isFirstColumn && !beforeHalf)) {
                uList.append(snapLinkItem);
            }
        });
        return snapTd;
    }

    function clearMessageBar() {
        AJS.$('#aui-message-bar').html("");
        AJS.$('#aui-message-bar').show();
    }

    function isStartTask() {
        return AJS.$('#vmwareTaskId').val() == "start";
    }

    function setSnapshotInputState() {
        if (getVmStartStateOptionValue() == 'CURRENT') {
            AJS.$('#vmware_vm_snapshot_name').val("");
            AJS.$('#vmware_vm_snapshot_name').attr('disabled', 'disabled');
            AJS.$('span.aui-icon', '#fieldLabelArea_vmware_vm_snapshot_name').hide();
        } else {
            AJS.$('#vmware_vm_snapshot_name').removeAttr('disabled');
            AJS.$('span.aui-icon', '#fieldLabelArea_vmware_vm_snapshot_name').show();
        }
    }

    function getVmStartStateOptionValue() {
        return AJS.$('input[name="vmware.start.vm.state"]:checked').val();
    }

    function setGuestStartTimeoutInputState() {
        if (AJS.$('#enableGuestStartTimeout').attr('checked')) {
            AJS.$('#vmware_guest_start_timeout_sec').removeAttr('disabled');
            AJS.$('span.aui-icon', '#fieldArea_vmware_guest_start_timeout_sec').show();
        } else {
            AJS.$('#vmware_guest_start_timeout_sec').val("");
            AJS.$('#vmware_guest_start_timeout_sec').attr('disabled', 'disabled');
            AJS.$('span.aui-icon', '#fieldArea_vmware_guest_start_timeout_sec').hide();
        }
    }

    AJS.$(document).ready(function() {
        if (isStartTask()) {
            setSnapshotInputState();
            setGuestStartTimeoutInputState();
            AJS.$('input[name="vmware.start.vm.state"]').click(function() {
                setSnapshotInputState();
            });
            AJS.$('#enableGuestStartTimeout').change(function () {
                setGuestStartTimeoutInputState();
            });
        }
    });

</script>