<input type="hidden" id="vmwareTaskId" value="stop">

[#include "commonVmSettings.ftl"]
[@ww.textfield labelKey="vmware.vm" name="vmware.vm" required='true'/]

[@ww.radio labelKey='vmware.stop.task.action'
                   name='vmware.stop.task.action'
                   listValue='name()' i18nPrefixForValue='vmware.stop.task.action.option'
                   toggle='true'
                   list=stopTaskActionOptions ]
[/@ww.radio]


[#include "availableVmTable.ftl"]