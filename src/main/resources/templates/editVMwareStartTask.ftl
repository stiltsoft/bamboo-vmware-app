<input type="hidden" id="vmwareTaskId" value="start">

[#include "commonVmSettings.ftl"]

[@ww.radio labelKey='vmware.start.vm.state'
            name='vmware.start.vm.state'
            listValue='name()' i18nPrefixForValue='vmware.start.vm.state.option'
            toggle='true'
            list=startVmStateOptions ]
[/@ww.radio]

<table border="0" style="margin-bottom: 10px;">
    <tr>
        <td>
            [@ww.textfield labelKey="vmware.vm" name="vmware.vm" required='true'/]
        </td>
        <td>
            [@ww.textfield labelKey="vmware.vm.snapshot.name" name="vmware.vm.snapshot.name" required='true'/]
        </td>
    </tr>
</table>

[#include "availableVmTable.ftl"]

[@ww.checkbox name="enableGuestStartTimeout" labelKey="bvm.enable.guest.start.timeout" fieldClass="guest-start-timeout-checkbox" /]
[@ww.textfield labelKey="vmware.guest.start.timeout.sec" name="vmware.guest.start.timeout.sec" required='true'/]

[#if !deploymentMode]
    [@ww.select
    listKey='name()'
    labelKey='vmware.post.build.complete.action' name='vmware.post.build.complete.action' cssClass="long-field"
    listValue='name()' i18nPrefixForValue='vmware.post.build.complete.action.option'
    list=postBuildOptions toggle=true /]
[/#if]
