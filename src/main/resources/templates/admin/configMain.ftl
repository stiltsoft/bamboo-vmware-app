[#-- @ftlvariable name="action" type="com.stiltsoft.bamboo.vmware.action.VMWareMainConfigAction" --]

[@dj.simpleDialogForm triggerSelector="#createVirtualHost" width=540 height=420 headerKey="bvm.vhost.create" submitCallback="redirectAfterReturningFromDialog" /]
[@dj.simpleDialogForm triggerSelector="#createAgentMapping" width=540 height=480 headerKey="bvm.agent.mapping.create.title" submitCallback="redirectAfterReturningFromDialog" /]
[@dj.simpleDialogForm triggerSelector=".edit-vhost" width=540 height=500 headerKey="bvm.vhost.edit" submitCallback="redirectAfterReturningFromDialog" /]
[@dj.simpleDialogForm triggerSelector=".edit-mapping" width=540 height=480 headerKey="bvm.agent.mapping.edit.title" submitCallback="redirectAfterReturningFromDialog" /]
[@dj.simpleDialogForm triggerSelector=".delete-vhost" width=540 height=420 headerKey="bvm.vhost.delete" submitCallback="redirectAfterReturningFromDialog" /]
[@dj.simpleDialogForm triggerSelector=".delete-mapping" width=540 height=420 headerKey="bvm.agent.mapping.delete.title" submitCallback="redirectAfterReturningFromDialog" /]
<html>
<head>
    <title>[@ww.text name='vmware.plugin.configure.title' /]</title>
    <meta name="decorator" content="adminpage">
</head>

<body>
    <h1>[@ww.text name='vmware.plugin.configure.title' /]</h1>
    <p>[@ww.text name='vmware.plugin.configure.form.title' /]</p>

    [@ww.actionerror /]
    [@ww.actionmessage /]

    <div class="aui-message">
        <p class="title">
            <span class="aui-icon icon-generic"></span>
            <strong>Virtual agent slots availability</strong>
        </p>
        <p>
            For correct operation of virtual agents, you need to have at least one empty slot for remote agents.
            <b>${action.availableAgentSlotCount}</b> slots are currently available.
        </p>
    </div>

    [@ui.bambooPanel titleKey='bvm.vagents.manage.panel.title' descriptionKey='bvm.vagents.manage.panel.description' cssClass='panel-container']
        [@ww.form action="reconfigureVirtualAgents" id="virtualAgentsConfiguration" cssClass="aui"]
        <p>${action.getText("bvm.vagents.available.slot.count")}:&nbsp;<b>${action.availableAgentSlotCount}</b><p/>

        <p>Virtual agent management:&nbsp;
            <b>
                [#if action.agentManagementEnabled]
                    <span class="bvm-green-text">Enabled</span>
                [#else]
                    <span class="bvm-red-text">Disabled</span>
                [/#if]
            </b>
        </p>
        <p>
        <span class="form-actions-bar">
            [@ww.text name='global.selection.action' /]:
            <span class="aui-buttons">
                [#if action.agentManagementEnabled]
                    [@ww.submit value=action.getText("bvm.vagents.disable") name="disableButton" id="enableVirtualAgentsButton" cssClass="aui-button" /]
                [#else]
                    [@ww.submit value=action.getText("bvm.vagents.enable") name="enableButton"   id="disableVirtualAgentsButton" cssClass="aui-button" /]
                [/#if]
            </span>
        </span>
        </p>
        [/@ww.form]
    [/@ui.bambooPanel]

    [@ui.bambooPanel titleKey='bvm.vhost.panel.title' descriptionKey='bvm.vhost.panel.description' cssClass='panel-container']
        [#if VHosts?has_content]
        <div style="width:90%">
        <table id="virtual-hosts" class="aui">
            <thead>
            <tr>
                <th>[@ww.text name='bvm.vhost.host.url' /]</th>
                <th>[@ww.text name='bvm.common.user.name' /]</th>
                <th>[@ww.text name='bvm.common.operations' /]</th>
            </tr>
            </thead>
            <tbody>
                [#foreach vHost in action.VHosts]
                <tr id="item-${vHost.id}">
                    <td class="labelPrefixCell">
                        <a href="${vHost.url}">${vHost.url}</a>
                    </td>
                    <td class="labelPrefixCell">
                        ${vHost.userName}
                    </td>
                    <td class="operations">
                        <a class="edit-vhost" href="[@ww.url action="configureVHost" namespace="/admin" /]?hostId=${vHost.id}">[@ww.text name="bvm.common.edit" /]</a> |
                        <a href="[@ww.url action="confirmVHostDelete" namespace="/admin"/]?hostId=${vHost.id}&returnUrl=${currentUrl}" class="delete-vhost" title="[@ww.text name='bvm.common.delete' /]">[@ww.text name="bvm.common.delete" /]</a>
                    </td>
                </tr>
                [/#foreach]
            </tbody>
        </table>
        </div>
        [#else]
            <p>There are currently no virtual hosts defined</p>
        [/#if]

        <div style="padding-top: 5px;">
            [@ww.url id='createVirtualHostUrl' value='/chain/admin/ajax/configureVHost.action' returnUrl=currentUrl lastModified=lastModified conditionKey=conditionKey notificationRecipientType=notificationRecipientType previousTypeData=previousTypeData /]
            [@cp.displayLinkButton buttonId='createVirtualHost' buttonLabel='bvm.vhost.create' buttonUrl=createVirtualHostUrl/]
        </div>
    [/@ui.bambooPanel]

    [@ui.bambooPanel titleKey='bvm.agent.mapping.panel.title' descriptionKey='bvm.agent.mapping.panel.description']
    <div class="aui-message">
        <p class="title">
            <span class="aui-icon icon-generic"></span>
            <strong>Note about enabling/disabling remote agents.</strong>
        </p>

        <p>
            The add-on will automatically enable remote agents upon start of the dedicated virtual machines.
            The add-on will also automatically disable remote agents and then shut down the dedicated virtual machines
            when there are no tasks in the queue for the running agents.
        </p>
    </div>
        [#if mappings?has_content]
        <div style="width:90%">
            <table id="agent-mappings" class="aui">
                <thead>
                <tr>
                    <th>[@ww.text name='bvm.agent.mapping.agent' /]</th>
                    <th>[@ww.text name='bvm.agent.mapping.vhost' /]</th>
                    <th>[@ww.text name='bvm.agent.mapping.vm' /]</th>
                    <th>[@ww.text name='bvm.agent.mapping.snapshot' /]</th>
                    <th>[@ww.text name='bvm.common.operations' /]</th>
                </tr>
                </thead>
                <tbody>
                    [#foreach mapping in action.mappings]
                    <tr id="item-${mapping.id}">
                        <td class="labelPrefixCell">
                            <a href="${req.contextPath}/admin/agent/viewAgent.action?agentId=${mapping.agentId}">${action.getAgentName(mapping.agentId)}</a>
                        </td>
                        <td class="labelPrefixCell">
                            ${action.getVHostData(mapping.VHostId)}
                        </td>
                        <td class="labelPrefixCell">
                        ${mapping.vmName}
                        </td>
                        <td class="labelPrefixCell">
                        ${mapping.snapshotName.or("")}
                        </td>
                        <td class="operations">
                            <a class="edit-mapping" href="[@ww.url action="mapAgentToVm" namespace="/admin" /]?mappingId=${mapping.id}">[@ww.text name="bvm.common.edit" /]</a> |
                            <a class="delete-mapping" href="[@ww.url action="confirmAgentToVmDelete" namespace="/admin"/]?mappingId=${mapping.id}&returnUrl=${currentUrl}" title="[@ww.text name='bvm.common.delete' /]">[@ww.text name="bvm.common.delete" /]</a>
                        </td>
                    </tr>
                    [/#foreach]
                </tbody>
            </table>
        </div>
        [#else]
        <p>There are currently no agent to vm mappings defined</p>
        [/#if]

    <div style="padding-top: 5px;">
        [@ww.url id='createAgentMappingUrl' value='/chain/admin/ajax/mapAgentToVm.action' returnUrl=currentUrl lastModified=lastModified conditionKey=conditionKey notificationRecipientType=notificationRecipientType previousTypeData=previousTypeData /]
            [@cp.displayLinkButton buttonId='createAgentMapping' buttonLabel='bvm.agent.mapping.create.title' buttonUrl=createAgentMappingUrl/]
    </div>
    [/@ui.bambooPanel]


</body>
</html>