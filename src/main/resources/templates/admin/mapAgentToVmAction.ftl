[#-- @ftlvariable name="action" type="com.stiltsoft.bamboo.vmware.action.MapAgentToVmAction" --]

<html>
<head>
${webResourceManager.requireResource("com.stiltsoft.bamboo.bamboo-vmware-plugin:selectThree")}
</head>
<body>

[@ww.form  action='mapAgentToVm!execute'
method='POST'
enctype='multipart/form-data'
namespace="/admin"
submitLabelKey="Save"
cancelUri="/admin/configureVMwarePluginMain.action"
cssClass="aui"]

    [@ww.hidden name="mappingId" value=action.mappingId /]

<div id="aui-message-bar"></div>
    [@ui.bambooSection id='mapping-params']
        [@ww.select name='agentId' labelKey='bvm.agent.mapping.agent' list=action.agents
        listKey='id' listValue='name' /]

        [@ww.select name='hostId' labelKey='bvm.agent.mapping.vhost' list=action.hosts
        listKey='id' listValue='url' /]

    [#--[#assign spinner]<span id="issue-loading-spinner">[@ui.icon type="loading" /]</span>[/#assign]--]
    [#--[@ww.textfield labelKey="Virtual machine name" name="vmName" id="vmName" required=true after=spinner /]--]

    <div class="field-group">
        <label for="dBase">${action.getText('bvm.agent.mapping.vm')}</label>
        [@ww.hidden name="vmName" value=action.vmName /]
        <span id="issue-loading-spinner">[@ui.icon type="loading" /]</span>
        [#if (fieldErrors['vmName'])?has_content]
            [#list fieldErrors['vmName'] as error]
                <div class="error control-form-error">${error?html}</div>
            [/#list]
        [/#if]
        <div class="description">${action.getText('bvm.agent.mapping.vm.description')}</div>
    </div>

        [#--[@ww.textfield labelKey="bvm.agent.mapping.snapshot" name="snapshotName" id="snapshotName" /]--]

    <div class="field-group">
        <label for="dBase">Snapshot</label>
        [@ww.hidden name="snapshotName" value=action.snapshotName /]
        <div class="description">${action.getText('bvm.agent.mapping.snapshot.description')}</div>
    </div>

        [@ww.checkbox name="stopAgentAfterBuild" labelKey="bvm.agent.mapping.stop.agent.after.build" /]


    [/@ui.bambooSection]

[/@ww.form]


<script type="text/javascript">
    (function ($) {
        AJS.toInit(function () {
            VAgents.MappingForm.init();
        });
    })(AJS.$);
</script>

</body>
</html>