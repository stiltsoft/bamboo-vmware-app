[#-- @ftlvariable name="" type="com.stiltsoft.bamboo.vmware.action.ConfigureVHostAction" --]

[@ww.form  action='configureVHost!execute'
method='POST'
enctype='multipart/form-data'
namespace="/admin"
submitLabelKey="Save"
cancelUri="/admin/configureVMwarePluginMain.action"
cssClass="aui"]

    [@ww.hidden name="hostId" value=hostId /]

    [@ui.bambooSection id='vhost-params']
        [@ww.textfield labelKey="bvm.vhost.host.url" name="hostUrl" id="hostUrl" required=true/]
        [@ww.textfield labelKey="bvm.vhost.host.user.name" name="hostUserName" id="hostUserName" required=true/]
        [@ww.password labelKey='bvm.vhost.password' name='hostPassword' id="hostPassword" required=true showPassword="true"/]
    <div class="field-group">
        <a id="testConnection" href="#">[@ww.text name="bvm.vhost.test.connection" /]</a>
    </div>
    [/@ui.bambooSection]

<div id="aui-message-bar"></div>
[/@ww.form]

<script type="text/javascript">
    (function ($) {
        function getHostParams() {
            var data = [];

            data.push("host=" + $('#hostUrl').val());
            data.push("&login=" + $('#hostUserName').val());
            data.push("&password=" + $('#hostPassword').val());

            return data.join("");
        }

        AJS.toInit(function () {
            $('#testConnection').click(function () {
                var messageContainer = $('#aui-message-bar');
                messageContainer.html("");
                VAgents.VMList.load(null, getHostParams());
                if (messageContainer.is(':empty')) {
                    var vmList = VAgents.VMList.getVMHostOptions().join(",");
                    AJS.messages.success({body: "<p>Available virtual machines: " + vmList + "</p>"});
                }
            });
        });
    })(AJS.$);
</script>