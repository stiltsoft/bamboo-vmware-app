[#-- @ftlvariable name="action" type="com.stiltsoft.bamboo.vmware.action.MapAgentToVmAction" --]

<title>[@ww.text name="Delete agent to vm mapping" /]</title>
[@ww.form action="mapAgentToVm!delete" namespace="/admin"
submitLabelKey="Delete"
id="confirmDelete"
cssClass="aui"]
<h3>Are you sure you want to delete agent to vm mapping?</h3>

    [@ww.label labelKey='bvm.agent.mapping.agent' escape="false"]
        [@ww.param name='value']
            <a href="${req.contextPath}/admin/agent/viewAgent.action?agentId=${action.agent.id}">${action.agent.name}</a>
        [/@ww.param]
    [/@ww.label]
    [@ww.label labelKey='bvm.agent.mapping.vhost.url' escape="false"]
        [@ww.param name='value']<a href="${action.VHost.url}">${action.VHost.url}</a>[/@ww.param]
    [/@ww.label]
    [@ww.label labelKey='bvm.common.user.name' name='VHost.userName' /]
    [@ww.label labelKey='vmware.vm' name='vmName' /]


    [@ww.hidden name="returnUrl" /]
    [@ww.hidden name="mappingId" /]
[/@ww.form]