[#-- @ftlvariable name="action" type="com.stiltsoft.bamboo.vmware.action.ConfigureVHostAction" --]

<title>[@ww.text name="Delete virtual host" /]</title>

[@ww.form action="configureVHost!delete" namespace="/admin"
submitLabelKey="Delete"
id="confirmDelete"
cssClass="aui"]
    [#if action.mappedAgents?has_content]
    <h3>Virtual host ${action.hostUrl} can't be delete.</h3>
    <p>${hostUrl} has following virtual agents:</p>
    <ul>
        [#foreach agent in action.mappedAgents]
            <li>
                <a href="${req.contextPath}/admin/agent/viewAgent.action?agentId=${agent.id}">${agent.name}</a>
            </li>
        [/#foreach]
    </ul>
    <script type="text/javascript">
        (function ($) {
            AJS.toInit(function () {
                $('#confirmDelete_save').hide();
            });
        })(AJS.$);
    </script>
    [#else]
    <h3>Are you sure you want to delete virtual host ${action.hostUrl} ?</h3>
    [/#if]

    [@ww.hidden name="returnUrl" /]
    [@ww.hidden name="hostId" /]
[/@ww.form]