(function ($) {

    window.VAgents = {

    };

    VAgents.MappingForm = {

        init: function () {
            var t = this;
            t.bindVmSelector();
            t.bindSnapshotSelector();
            t.bindSelect2CloseHelper();
            t.bindVHostChange();

            VAgents.VMList.init();
            if (t.getHostId()) {
                VAgents.VMList.load(t.getHostId());
            }
        },

        bindVmSelector: function() {
            var t = this;
            //https://github.com/ivaynberg/select2/issues/1345
            $("#mapAgentToVm_vmName").select2({
                // aui-dropdown2 приводит к непредсказуемым последствиям
                containerCssClass: "bvm-aui-select2-container",
                dropdownCssClass: "aui-select2-drop aui-style-default",
                initSelection: function (element, callback) {
                    var data = {id: element.val(), text: element.val()};
                    callback(data);
                },
                query: function (query) {
                    var data = {results: []};
                    var includeTerm = true;

                    $.each(VAgents.VMList.getVMHostOptions(), function (i, v) {
                        if (query.term.length == 0 || v.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                            data.results.push({id: v, text: v });
                            includeTerm = false;
                        }
                    });

                    if (includeTerm) {
                        data.results.push({id: query.term, text: query.term});
                    }

                    query.callback(data);
                }
            }).on("change", function(e) {
                t.resetSnapshotList();
            });
        },

        bindSnapshotSelector: function() {
            //https://github.com/ivaynberg/select2/issues/1345
            $("#mapAgentToVm_snapshotName").select2({
                // aui-dropdown2 приводит к непредсказуемым последствиям
                containerCssClass: "bvm-aui-select2-container",
                dropdownCssClass: "aui-select2-drop aui-style-default",
                initSelection: function (element, callback) {
                    var data = {id: element.val(), text: element.val()};
                    callback(data);
                },
                query: function (query) {
                    var data = {results: []};
                    var includeTerm = true;

                    $.each(VAgents.VMList.getSnapshotOptions(), function (i, v) {
                        if (query.term.length == 0 || v.toUpperCase().indexOf(query.term.toUpperCase()) >= 0) {
                            data.results.push({id: v, text: v });
                            includeTerm = false;
                        }
                    });

                    if (includeTerm) {
                        data.results.push({id: query.term, text: query.term});
                    }

                    query.callback(data);
                }
            });
        },

        bindSelect2CloseHelper: function() {
            $(document).mousedown(function (e) {
                var input = $(".select2-container");
                var container = $("#select2-drop");
                var select = $("#mapAgentToVm_vmName");

                if (!input.is(e.target) && input.has(e.target).length === 0) {
                    if (!container.is(e.target) && container.has(e.target).length === 0) {
                        select.select2("close");
                    }
                }
            });
        },

        bindVHostChange: function() {
            var t = this;
            $("#mapAgentToVm_hostId").change(function () {
                t.resetVmList();
                VAgents.VMList.load(t.getHostId());
            });
        },

        resetVmList: function () {
            $("#mapAgentToVm_vmName").select2("val", "").trigger("change");
        },

        resetSnapshotList: function () {
            $('#mapAgentToVm_snapshotName').select2("val", "").trigger("change");
        },

        getHostId: function() {
            return $('#mapAgentToVm_hostId').val();
        }

    };

    VAgents.VMList = {

        vmOptions: [],
        vmByName: {},

        init: function() {
            this.getVmLoadingSpinner().hide();
        },

        load: function (hostId, hostParams) {
            var t = this;
            $.ajax({
                async: false,
                type: 'POST',
                url: BAMBOO.contextPath + '/plugins/vmware/getAvailableVMs.action',
                data: hostId ? t.getVMHostParams(hostId) : hostParams,
                dataType: 'json',
                before: t.doBeforeSend(),
                success: t.handleSuccessResult,
                error: t.handleErrorResult,
                complete: t.doAfterComplete
            });
        },

        getVMHostParams: function (hostId) {
            return "hostId=" + hostId;
        },

        handleSuccessResult: function (data) {
            var t = VAgents.VMList;
            t.vmOptions = [];
            if (data.error) {
                t.handleErrorResult("", "", data.error);
                return;
            }

            $.each(data.vms, function (i, v) {
                var o = $.parseJSON(v);
                t.vmOptions.push(o.name);
                t.vmByName[o.name] = o;
            });

        },

        handleErrorResult: function (xhr, status, errorThrown) {
            $('#aui-message-bar').html("");
            AJS.messages.error({body: "<p>Unable to get vm list: " + errorThrown + "</p>"});
        },

        doBeforeSend: function () {
            var t = VAgents.VMList;
            t.getVmLoadingSpinner().show();
        },

        doAfterComplete: function () {
            var t = VAgents.VMList;
            t.getVmLoadingSpinner().hide();
        },

        getVmLoadingSpinner: function () {
            return $("#issue-loading-spinner");
        },

        getVMHostOptions: function () {
            return this.vmOptions;
        },

        getSnapshotOptions: function () {
            var t = this;
            var vmName = t.getVmName();
            var vm = t.vmByName[vmName];
            return vm ? vm.snapshotNames : [];
        },

        getVmName: function() {
            return $('#mapAgentToVm_vmName').val();
        }

    };

})(AJS.$);