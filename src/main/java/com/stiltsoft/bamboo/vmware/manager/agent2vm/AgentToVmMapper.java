package com.stiltsoft.bamboo.vmware.manager.agent2vm;

import java.util.Collection;

public interface AgentToVmMapper {

    AgentToVmMapping store(AgentToVmMapping mapping);

    AgentToVmMapping load(String mappingId);

    void remove(AgentToVmMapping mapping);

    Collection<AgentToVmMapping> loadAll();

}
