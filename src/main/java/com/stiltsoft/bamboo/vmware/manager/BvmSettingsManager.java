package com.stiltsoft.bamboo.vmware.manager;

import com.stiltsoft.bamboo.vmware.persistence.PersistenceManager;

import static org.apache.commons.lang.StringUtils.isEmpty;

public class BvmSettingsManager {

    private static final String AGENT_MANAGEMENT_ENABLED_KEY = "stiltsoft.bvm.settings.agentman.enabled";

    private PersistenceManager persistenceManager;

    private volatile boolean agentManagementEnabled;

    public BvmSettingsManager(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
        agentManagementEnabled = loadAgentManagementEnabled();
    }

    public boolean isAgentManagementEnabled() {
        return agentManagementEnabled;
    }

    public void setAgentManagementEnabled(boolean agentManagementEnabled) {
        this.agentManagementEnabled = agentManagementEnabled;
        persistenceManager.set(AGENT_MANAGEMENT_ENABLED_KEY, Boolean.toString(agentManagementEnabled));
    }

    private boolean loadAgentManagementEnabled() {
        String enabledStr = persistenceManager.get(AGENT_MANAGEMENT_ENABLED_KEY);
        return isEmpty(enabledStr) || Boolean.parseBoolean(enabledStr);
    }

}
