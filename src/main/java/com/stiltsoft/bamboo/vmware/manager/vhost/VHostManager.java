package com.stiltsoft.bamboo.vmware.manager.vhost;

import java.util.Collection;

public interface VHostManager {

    VHost store(VHost vHost);

    VHost load(String id);

    void remove(VHost vHost);

    Collection<VHost> loadAll();

}