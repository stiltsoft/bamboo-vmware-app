package com.stiltsoft.bamboo.vmware.manager.vhost;

import com.stiltsoft.bamboo.vmware.persistence.PersistenceManager;
import com.stiltsoft.bamboo.vmware.util.IdGenerator;
import org.springframework.beans.factory.InitializingBean;

import java.util.*;

public class DefaultVHostManager implements VHostManager, InitializingBean {

    private static final String V_HOST_ID_PREFIX = "stiltsoft.bvm.vhost";

    private PersistenceManager persistenceManager;

    private Map<String, VHost> vHostById = new HashMap<String, VHost>();

    public DefaultVHostManager(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

    @Override
    public VHost store(VHost vHost) {
        String id = vHost.getId();

        if (id == null) {
            id = generateVHostId();
            vHost.setId(id);
        }

        vHostById.put(id, vHost);
        persistenceManager.set(id, vHost.serialize());

        return vHost;
    }

    @Override
    public VHost load(String id) {
        return vHostById.get(id);
    }

    @Override
    public void remove(VHost vHost) {
        persistenceManager.remove(vHost.getId());
        vHostById.remove(vHost.getId());
    }

    @Override
    public Collection<VHost> loadAll() {
        return vHostById.values();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        populateCache();
    }

    private void populateCache() {
        List<String> ids = findIds();
        for (String id : ids) {
            VHost vHost = loadVHostFromStorage(id);
            vHostById.put(id, vHost);
        }
    }

    private List<String> findIds() {
        List<String> keys = new ArrayList<String>();
        for (String key : persistenceManager.getKeys()) {
            if (key.startsWith(V_HOST_ID_PREFIX)) {
                keys.add(key);
            }
        }
        return keys;
    }

    private VHost loadVHostFromStorage(String id) {
        String vHostStr = persistenceManager.get(id);
        VHost vHost = VHost.deserialize(vHostStr);
        vHost.setId(id);
        return vHost;
    }

    private String generateVHostId() {
        return IdGenerator.generate(V_HOST_ID_PREFIX);
    }

}
