package com.stiltsoft.bamboo.vmware.manager.vhost;

import com.stiltsoft.bamboo.vmware.util.MapSerializer;

import java.util.HashMap;
import java.util.Map;

public class VHost {

    private static final String URL_FIELD_NAME = "url";
    private static final String USER_NAME_FIELD_NAME = "user";
    private static final String PASSWORD_FIELD_NAME = "pass";


    private String id = null;
    private String url;
    private String userName;
    private String password;

    public VHost(String url, String userName, String password) {
        this.url = url;
        this.userName = userName;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String serialize() {
        Map<String, String> fieldByName = new HashMap<String, String>(3);
        fieldByName.put(URL_FIELD_NAME, getUrl());
        fieldByName.put(USER_NAME_FIELD_NAME, getUserName());
        fieldByName.put(PASSWORD_FIELD_NAME, getPassword());
        return MapSerializer.serialize(fieldByName);
    }

    public static VHost deserialize(String vHostStr) {
        Map<String, String> fieldByName = MapSerializer.deSerialize(vHostStr);
        return new VHost(fieldByName.get(URL_FIELD_NAME),
                fieldByName.get(USER_NAME_FIELD_NAME),
                fieldByName.get(PASSWORD_FIELD_NAME));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        VHost vHost = (VHost) o;

        return !(id != null ? !id.equals(vHost.id) : vHost.id != null) &&
                password.equals(vHost.password) &&
                url.equals(vHost.url) &&
                userName.equals(vHost.userName);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + url.hashCode();
        result = 31 * result + userName.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "VHost{" +
                "id='" + id + '\'' +
                ", url='" + url + '\'' +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

}
