package com.stiltsoft.bamboo.vmware.manager.agent2vm;

import com.stiltsoft.bamboo.vmware.persistence.PersistenceManager;
import com.stiltsoft.bamboo.vmware.util.IdGenerator;
import org.springframework.beans.factory.InitializingBean;

import java.util.*;

public class DefaultAgentToVmMapper implements AgentToVmMapper, InitializingBean {

    private static final String MAPPING_ID_PREFIX = "stiltsoft.bvm.amap";

    private PersistenceManager persistenceManager;

    private Map<String, AgentToVmMapping> mappingById = new HashMap<String, AgentToVmMapping>();

    public DefaultAgentToVmMapper(PersistenceManager persistenceManager) {
        this.persistenceManager = persistenceManager;
    }

    @Override
    public AgentToVmMapping store(AgentToVmMapping mapping) {
        String id = mapping.getId();

        if (id == null) {
            id = generateMappingId();
            mapping.setId(id);
        }

        mappingById.put(id, mapping);
        persistenceManager.set(id, mapping.serialize());

        return mapping;
    }

    @Override
    public AgentToVmMapping load(String mappingId) {
        return mappingById.get(mappingId);
    }

    @Override
    public void remove(AgentToVmMapping mapping) {
        persistenceManager.remove(mapping.getId());
        mappingById.remove(mapping.getId());
    }

    @Override
    public Collection<AgentToVmMapping> loadAll() {
        return mappingById.values();
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        populateCache();
    }

    private void populateCache() {
        List<String> ids = findIds();
        for (String id : ids) {
            AgentToVmMapping mapping = loadMappingFromStorage(id);
            mappingById.put(id, mapping);
        }
    }

    private List<String> findIds() {
        List<String> keys = new ArrayList<String>();
        for (String key : persistenceManager.getKeys()) {
            if (key.startsWith(MAPPING_ID_PREFIX)) {
                keys.add(key);
            }
        }
        return keys;
    }

    private AgentToVmMapping loadMappingFromStorage(String id) {
        String mappingStr = persistenceManager.get(id);
        AgentToVmMapping mapping = AgentToVmMapping.deserialize(mappingStr);
        mapping.setId(id);
        return mapping;
    }

    private String generateMappingId() {
        return IdGenerator.generate(MAPPING_ID_PREFIX);
    }

}
