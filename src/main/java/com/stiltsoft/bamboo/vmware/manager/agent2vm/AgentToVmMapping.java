package com.stiltsoft.bamboo.vmware.manager.agent2vm;

import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.util.MapSerializer;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Optional.fromNullable;
import static org.apache.commons.lang.StringUtils.isNotBlank;

public class AgentToVmMapping {

    private static final String V_HOST_ID_FIELD_NAME = "url";
    private static final String VM_NAME_FIELD_NAME = "user";
    private static final String AGENT_ID_FIELD_NAME = "pass";
    private static final String SNAPSHOT_NAME = "snapshot";
    private static final String STOP_AFTER_BUILD = "stop";

    private String id;
    private String vHostId;
    private String vmName;
    private long agentId;
    private String snapshotName;
    private boolean stopAgentAfterBuild;

    public AgentToVmMapping(String vHostId,
                            String vmName,
                            long agentId,
                            String snapshotName,
                            boolean stopAgentAfterBuild) {

        this.vHostId = vHostId;
        this.vmName = vmName;
        this.agentId = agentId;
        this.stopAgentAfterBuild = stopAgentAfterBuild;
        this.snapshotName = isNotBlank(snapshotName) ? snapshotName : null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVHostId() {
        return vHostId;
    }

    public String getVmName() {
        return vmName;
    }

    public long getAgentId() {
        return agentId;
    }

    public Optional<String> getSnapshotName() {
        return fromNullable(snapshotName);
    }

    public boolean isStopAgentAfterBuild() {
        return stopAgentAfterBuild;
    }

    public String serialize() {
        Map<String, String> fieldByName = new HashMap<String, String>(3);
        fieldByName.put(V_HOST_ID_FIELD_NAME, getVHostId());
        fieldByName.put(VM_NAME_FIELD_NAME, getVmName());
        fieldByName.put(AGENT_ID_FIELD_NAME, Long.toString(getAgentId()));

        if (getSnapshotName().isPresent()) {
            fieldByName.put(SNAPSHOT_NAME, getSnapshotName().get());
        }

        if (isStopAgentAfterBuild()) {
            fieldByName.put(STOP_AFTER_BUILD, "true");
        }
        return MapSerializer.serialize(fieldByName);
    }

    public static AgentToVmMapping deserialize(String agentToVmMapping) {
        Map<String, String> fieldByName = MapSerializer.deSerialize(agentToVmMapping);
        return new AgentToVmMapping(fieldByName.get(V_HOST_ID_FIELD_NAME),
                fieldByName.get(VM_NAME_FIELD_NAME),
                Long.valueOf(fieldByName.get(AGENT_ID_FIELD_NAME)),
                fieldByName.get(SNAPSHOT_NAME),
                fieldByName.get(STOP_AFTER_BUILD) != null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AgentToVmMapping that = (AgentToVmMapping) o;

        return agentId == that.agentId && !(id != null ? !id.equals(that.id) : that.id != null) &&
                vHostId.equals(that.vHostId) &&
                vmName.equals(that.vmName);
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + vHostId.hashCode();
        result = 31 * result + vmName.hashCode();
        result = 31 * result + (int) (agentId ^ (agentId >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "AgentToVmMapping{" +
                "id='" + id + '\'' +
                ", vHostId='" + vHostId + '\'' +
                ", vmName='" + vmName + '\'' +
                ", agentId=" + agentId +
                '}';
    }

}
