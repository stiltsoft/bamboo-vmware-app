package com.stiltsoft.bamboo.vmware.util;

import com.google.common.base.Function;
import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class MapSerializer {

    private static final String ENCODING = "UTF-8";
    private static final String KEY_VALUE_SEPARATOR = "=";
    private static final String PAIRS_SEPARATOR = "&";

    private static final Encoder ENCODER = new Encoder();
    private static final Decoder DECODER = new Decoder();

    public static String serialize(Map<String, String> input) {
        Map<String, String> encodedInput = encode(input);
        return Joiner.on(PAIRS_SEPARATOR)
                .withKeyValueSeparator(KEY_VALUE_SEPARATOR)
                .join(encodedInput);
    }

    public static Map<String, String> deSerialize(String input) {
        Map<String, String> encodedMap = Splitter.on(PAIRS_SEPARATOR)
                .omitEmptyStrings()
                .withKeyValueSeparator(KEY_VALUE_SEPARATOR)
                .split(input);

        return decode(encodedMap);
    }

    /**
     * https://code.google.com/p/guava-libraries/issues/detail?id=1470
     */
    private static Map<String, String> encode(Map<String, String> input) {
        return modifyMap(input, ENCODER);
    }

    private static Map<String, String> decode(Map<String, String> input) {
        return modifyMap(input, DECODER);
    }

    private static Map<String, String> modifyMap(Map<String, String> input, Function<String, String> fn) {
        Map<String, String> result = new HashMap<String, String>(input.size());

        for (Map.Entry<String, String> entry : input.entrySet()) {
            result.put(fn.apply(entry.getKey()), fn.apply(entry.getValue()));
        }

        return result;
    }

    private static class Encoder implements Function<String, String> {
        @Override
        public String apply(String input) {
            try {
                return URLEncoder.encode(input, ENCODING);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private static class Decoder implements Function<String, String> {
        @Override
        public String apply(String input) {
            try {
                return URLDecoder.decode(input, ENCODING);
            } catch (UnsupportedEncodingException e) {
                throw new RuntimeException(e);
            }
        }
    }

}