package com.stiltsoft.bamboo.vmware.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.FieldDictionary;
import com.thoughtworks.xstream.converters.reflection.ImmutableFieldKeySorter;
import com.thoughtworks.xstream.converters.reflection.Sun14ReflectionProvider;
import com.thoughtworks.xstream.io.xml.DomDriver;

public class DebugHelper {

    private static XStream xStream;

    public String objectToXmlString(Object obj) {
        return getXStream().toXML(obj);
    }

    private static XStream getXStream() {
        if (xStream == null) {
            xStream = new XStream(
                    new Sun14ReflectionProvider(new FieldDictionary(new ImmutableFieldKeySorter())),
                    new DomDriver("utf-8"));
        }
        return xStream;
    }

}
