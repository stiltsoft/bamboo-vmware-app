package com.stiltsoft.bamboo.vmware.util;

import static java.lang.System.currentTimeMillis;

public class IdGenerator {

    public static synchronized String generate(String idPrefix) {
        return idPrefix + currentTimeMillis();
    }

}
