package com.stiltsoft.bamboo.vmware.service;

import java.util.List;

public interface VMwareService {

    public List<VMachineInfo> getVMachineInfoList() throws VMachineOperationException;

    public VMachine getVirtualMachine(String name) throws VMachineOperationException;

}
