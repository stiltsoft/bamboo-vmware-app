package com.stiltsoft.bamboo.vmware.service;

import com.stiltsoft.bamboo.vmware.manager.vhost.VHost;
import com.vmware.vim25.AboutInfo;
import com.vmware.vim25.mo.*;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class DefaultVMwareService implements VMwareService {

    private ServiceInstance serviceInstance;

    public DefaultVMwareService(String url, String username, String password) throws MalformedURLException, RemoteException {
        this.serviceInstance = new ServiceInstance(new URL(url), username, password, true);
    }

    public DefaultVMwareService(VHost vHost) throws MalformedURLException, RemoteException {
        this(vHost.getUrl(), vHost.getUserName(), vHost.getPassword());
    }

    public List<VMachineInfo> getVMachineInfoList() throws VMachineOperationException {
        List<VMachineInfo> vmInfoList = new ArrayList<VMachineInfo>();
        ManagedEntity[] mes;

        try {
            mes = new InventoryNavigator(getRootFolder()).searchManagedEntities("VirtualMachine");
        } catch (RemoteException e) {
            throw new VMachineOperationException(e);
        }

        if (mes != null) {
            for (ManagedEntity me : mes) {
                vmInfoList.add(new VMachine((VirtualMachine) me).getInfo());
            }
        }
        return vmInfoList;
    }

    public VMachine getVirtualMachine(String name) throws VMachineOperationException {
        ManagedEntity me;
        try {
            me = new InventoryNavigator(getRootFolder()).searchManagedEntity("VirtualMachine", name);
        } catch (RemoteException e) {
            throw new VMachineOperationException(e);
        }

        if (me != null && me instanceof VirtualMachine) {
            return new VMachine((VirtualMachine) me);
        } else {
            return null;
        }
    }

    public void disconnect() {
        serviceInstance.getServerConnection().logout();
    }

    public String getPlatformName() {
        return getAboutInfo().getName();
    }

    public String getPlatformVersion() {
        return getAboutInfo().getVersion();
    }

    private AboutInfo getAboutInfo() {
        return serviceInstance.getAboutInfo();
    }

    private Folder getRootFolder() {
        return serviceInstance.getRootFolder();
    }

}