package com.stiltsoft.bamboo.vmware.service;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class VMachineInfo {

    private String name;
    private int cpuCount;
    private int memorySizeMb;
    private List<String> snapshotNames;

    public VMachineInfo(String name, int cpuCount, int memorySizeMb, List<String> snapshotNames) {
        this.name = name;
        this.cpuCount = cpuCount;
        this.memorySizeMb = memorySizeMb;
        this.snapshotNames = snapshotNames;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        JSONObject json = new JSONObject();
        try {
            json.put("name", name);
            json.put("cpuCount", cpuCount);
            json.put("memorySizeMb", memorySizeMb);
            json.put("snapshotNames", snapshotNames);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return json.toString();
    }

}
