package com.stiltsoft.bamboo.vmware.service;

import com.stiltsoft.bamboo.vmware.task.VMwareActionException;
import com.vmware.vim25.*;
import com.vmware.vim25.mo.Task;
import com.vmware.vim25.mo.VirtualMachine;
import com.vmware.vim25.mo.VirtualMachineSnapshot;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.Nullable;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import static com.stiltsoft.bamboo.vmware.service.VMachine.PowerTaskType.*;
import static com.vmware.vim25.TaskInfoState.success;
import static com.vmware.vim25.VirtualMachinePowerState.*;
import static java.lang.System.currentTimeMillis;
import static java.lang.Thread.sleep;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

public class VMachine {

    private static final Logger log = Logger.getLogger(VMachine.class);

    private VirtualMachine virtualMachine;

    private List<String> snapshotNames;

    public VMachine(VirtualMachine virtualMachine) {
        this.virtualMachine = virtualMachine;
    }

    public String getName() {
        return virtualMachine.getName();
    }

    private VirtualMachinePowerState getPowerState() {
        return virtualMachine.getRuntime().getPowerState();
    }

    public boolean isPoweredOn() {
        return poweredOn == getPowerState();
    }

    public boolean isPoweredOff() {
        return poweredOff == getPowerState();
    }

    public boolean isSuspended() {
        return suspended == getPowerState();
    }

    public int getCpuCount() {
        return virtualMachine.getSummary().getConfig().getNumCpu();
    }

    public int getMemorySizeMB() {
        return virtualMachine.getSummary().getConfig().getMemorySizeMB();
    }

    public VMachineInfo getInfo() {
        return new VMachineInfo(getName(), getCpuCount(), getMemorySizeMB(), getSnapshotNames());
    }

    public TaskInfoState powerOn() throws VMachineOperationException {
        return executePowerTask(POWER_ON);
    }

    public TaskInfoState powerOff() throws VMachineOperationException {
        return executePowerTask(POWER_OFF);
    }

    public TaskInfoState suspend() throws VMachineOperationException {

        return executePowerTask(SUSPEND);
    }

    public TaskInfoState revertToSnapshot(VirtualMachineSnapshot vmSnap) throws VMachineOperationException {
        try {
            //http://www.vmware.com/support/developer/vc-sdk/visdk41pubs/ApiReference/vim.vm.Snapshot.html
            return executeVmTask(vmSnap.revertToSnapshot_Task(null));
        } catch (Exception e) {
            throw new VMachineOperationException("Unable to revert vm to snapshot.", e);
        }
    }

    public TaskInfoState checkedPowerOn() throws VMachineOperationException {
        if (isPoweredOn()) {
            log.warn("Virtual machine '" + getName() + "' is already started.");
            return success;
        } else {
            return powerOn();
        }
    }

    public TaskInfoState checkedPowerOff() throws VMachineOperationException {
        if (isPoweredOff()) {
            return success;
        } else {
            return powerOff();
        }
    }

    public TaskInfoState checkedSuspend() throws VMachineOperationException {
        if (isPoweredOff()) {
            log.warn("Can't suspend virtual machine '" + getName() + "' because it is powered off.");
            return success;
        } else if (isSuspended()) {
            log.warn("Virtual machine '" + getName() + "' has been already suspended.");
            return success;
        } else {
            return suspend();
        }
    }

    public void shutdownGuest() throws VMachineOperationException {
        try {
            if (isPoweredOn()) {
                log.warn("VM '" + virtualMachine.getName() + "' tools status: " + virtualMachine.getGuest().getToolsStatus());
                virtualMachine.shutdownGuest();
            }
        } catch (RemoteException e) {
            throw new VMachineOperationException("Unable to shutdown guest OS.", e);

        }
    }

    public WaitForStartResult waitForGuestToStart(long timeoutMs) throws VMachineOperationException {
        log.info("Guest OS start waiting started. VM: " + getName() + "; TimeoutMs: " + timeoutMs);

        WaitForStartResult result = new WaitForStartResult();
        long timeout = currentTimeMillis() + timeoutMs;

        try {
            boolean isVMWareToolsRunning;
            do {
                isVMWareToolsRunning = isVMWareToolsRunning();
                sleep(1000);
            } while (!isVMWareToolsRunning && currentTimeMillis() < timeout);

            result.setVmWareToolsRunning(isVMWareToolsRunning);

            boolean isHostNameDetected = false;
            if (isVMWareToolsRunning) {
                do {
                    isHostNameDetected = isHostNameDetected();
                    sleep(1000);
                } while (!isHostNameDetected && currentTimeMillis() < timeout);
            }

            result.setHostNameDetected(isHostNameDetected);
        } catch (InterruptedException e) {
            throw new VMachineOperationException(e);
        }

        long completeIn = currentTimeMillis() - (timeout - timeoutMs);
        log.info("Guest OS start waiting finished. Time: " + completeIn + "; " + result);
        return result;
    }

    public boolean waitForGuestToShutDown(long timeoutMs) throws VMachineOperationException {
        log.info("Guest OS shutdown waiting started. VM: " + getName() + "; TimeoutMs: " + timeoutMs);
        try {
            long timeout = currentTimeMillis() + timeoutMs;

            boolean isPoweredOff;
            do {
                isPoweredOff = isPoweredOff();
                sleep(1000);
            } while (!isPoweredOff && currentTimeMillis() < timeout);

            long completeIn = currentTimeMillis() - (timeout - timeoutMs);
            log.info("Guest OS shutdown waiting finished. Successful: " + isPoweredOff + "; Time: " + completeIn);
            return isPoweredOff;

        } catch (InterruptedException e) {
            throw new VMachineOperationException(e);
        }
    }

    @Nullable
    public VirtualMachineSnapshot getSnapshot(String snapName) {
        VirtualMachineSnapshotTree[] snapTree = getRootSnapshotList();
        if (snapTree != null) {
            ManagedObjectReference mor = findSnapshotInTree(snapTree, snapName);
            if (mor != null) {
                return new VirtualMachineSnapshot(virtualMachine.getServerConnection(), mor);
            }
        }
        return null;
    }

    public List<String> getSnapshotNames() {
        snapshotNames = new ArrayList<String>();
        VirtualMachineSnapshotTree[] snapTree = getRootSnapshotList();
        if (snapTree != null) {
            populateSnapshotNames(snapTree);
        }
        return snapshotNames;
    }

    public TaskInfoState revertToSnapshot(String snapshotName)
            throws VMachineOperationException, VMwareActionException {

        VirtualMachineSnapshot snapshot = getSnapshot(snapshotName);

        if (snapshot != null) {
            return revertToSnapshot(snapshot);
        } else {
            throw new VMwareActionException("Couldn't revert virtual machine to snapshot. Snapshot '"
                    + snapshotName + "' is not found.");
        }
    }

    /**
     * https://www.vmware.com/support/developer/converter-sdk/conv55_apireference/vim.vm.GuestInfo.html
     * https://www.vmware.com/support/developer/converter-sdk/conv55_apireference/vim.vm.GuestInfo.ToolsRunningStatus.html
     */
    private boolean isVMWareToolsRunning() {
        GuestInfo guestInfo = virtualMachine.getGuest();
        if (guestInfo != null) {
            boolean isToolsRunning = "guestToolsRunning".equalsIgnoreCase(guestInfo.getToolsRunningStatus());
            if (isToolsRunning) {
                return true;
            }
        }
        return false;
    }

    private boolean isHostNameDetected() {
        GuestInfo guestInfo = virtualMachine.getGuest();
        if (guestInfo != null) {
            String hostName = guestInfo.getHostName();
            return isNotEmpty(hostName);
        } else {
            return false;
        }
    }

    private TaskInfoState executePowerTask(PowerTaskType powerTaskType) throws VMachineOperationException {
        try {
            Task powerTask;

            switch (powerTaskType) {
                case POWER_ON:
                    powerTask = virtualMachine.powerOnVM_Task(null);
                    break;
                case POWER_OFF:
                    powerTask = virtualMachine.powerOffVM_Task();
                    break;
                case SUSPEND:
                    powerTask = virtualMachine.suspendVM_Task();
                    break;
                default:
                    throw new UnsupportedOperationException(powerTaskType.name());
            }

            return executeVmTask(powerTask);
        } catch (Exception e) {
            throw new VMachineOperationException("Unable to execute vm power task of type " + powerTaskType + "'", e);
        }
    }

    private TaskInfoState executeVmTask(final Task powerTask) {
        try {
            return TaskInfoState.valueOf(powerTask.waitForTask());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static ManagedObjectReference findSnapshotInTree(VirtualMachineSnapshotTree[] snapTree, String snapName) {
        for (VirtualMachineSnapshotTree node : snapTree) {
            if (snapName.equals(node.getName())) {
                return node.getSnapshot();
            } else {
                VirtualMachineSnapshotTree[] childTree = node.getChildSnapshotList();
                if (childTree != null) {
                    ManagedObjectReference mor = findSnapshotInTree(childTree, snapName);
                    if (mor != null) {
                        return mor;
                    }
                }
            }
        }
        return null;
    }

    private void populateSnapshotNames(VirtualMachineSnapshotTree[] snapTree) {
        for (VirtualMachineSnapshotTree node : snapTree) {
            snapshotNames.add(node.getName());
            VirtualMachineSnapshotTree[] childTree = node.getChildSnapshotList();
            if (childTree != null) {
                populateSnapshotNames(childTree);
            }
        }
    }

    @Nullable
    private VirtualMachineSnapshotTree[] getRootSnapshotList() {
        VirtualMachineSnapshotInfo snapInfo = virtualMachine.getSnapshot();
        if (snapInfo != null) {
            return snapInfo.getRootSnapshotList();
        } else {
            return null;
        }
    }

    protected enum PowerTaskType {
        POWER_ON,
        POWER_OFF,
        SUSPEND
    }

    public static class WaitForStartResult {

        boolean vmWareToolsRunning = false;
        boolean hostNameDetected = false;

        public boolean isVmWareToolsRunning() {
            return vmWareToolsRunning;
        }

        public void setVmWareToolsRunning(boolean vmWareToolsRunning) {
            this.vmWareToolsRunning = vmWareToolsRunning;
        }

        public boolean isHostNameDetected() {
            return hostNameDetected;
        }

        public void setHostNameDetected(boolean hostNameDetected) {
            this.hostNameDetected = hostNameDetected;
        }

        @Override
        public String toString() {
            return "WaitForStartResult{" +
                    "vmWareToolsRunning=" + vmWareToolsRunning +
                    ", hostNameDetected=" + hostNameDetected +
                    '}';
        }
    }

}