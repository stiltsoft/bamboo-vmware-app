package com.stiltsoft.bamboo.vmware.service;

public class VMachineOperationException extends RuntimeException {

    public VMachineOperationException(String message, Throwable cause) {
        super(message, cause);
    }

    public VMachineOperationException(Throwable cause) {
        super(cause);
    }

}
