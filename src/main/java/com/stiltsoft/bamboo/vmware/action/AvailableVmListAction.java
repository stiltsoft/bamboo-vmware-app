package com.stiltsoft.bamboo.vmware.action;

import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHost;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHostManager;
import com.stiltsoft.bamboo.vmware.service.DefaultVMwareService;
import com.stiltsoft.bamboo.vmware.service.VMachineInfo;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.List;

import static org.apache.struts2.ServletActionContext.getRequest;
import static org.apache.struts2.ServletActionContext.getResponse;

public class AvailableVmListAction extends BambooActionSupport {

    private static final Logger log = Logger.getLogger(AvailableVmListAction.class);

    private VHostManager vHostManager;

    @SuppressWarnings({"UnusedDeclaration"})
    public void getAvailableVMs() throws JSONException, IOException {
        JSONObject json = new JSONObject();
        try {
            json.put("vms", getVMachineInfoList());
        } catch (Exception e) {
            log.error("Unable to get VMs list.", e);
            String errorMessage = e.getMessage() != null ? e.getMessage() : e.getClass().getSimpleName();
            json.put("error", errorMessage);
        }

        getResponse().setContentType("application/json;charset=UTF-8");
        getResponse().getWriter().println(json.toString());
    }

    private List<VMachineInfo> getVMachineInfoList() throws MalformedURLException, RemoteException {
        String host;
        String login;
        String password;

        String hostId = getRequest().getParameter("hostId");
        if (StringUtils.isNotEmpty(hostId)) {
            VHost vHost = vHostManager.load(hostId);
            host = vHost.getUrl();
            login = vHost.getUserName();
            password = vHost.getPassword();
        } else {
            host = getRequest().getParameter("host");
            login = getRequest().getParameter("login");
            password = getRequest().getParameter("password");
        }

        return new DefaultVMwareService(host, login, password).getVMachineInfoList();
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setvHostManager(VHostManager vHostManager) {
        this.vHostManager = vHostManager;
    }

}
