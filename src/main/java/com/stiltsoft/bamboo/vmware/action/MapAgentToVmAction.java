package com.stiltsoft.bamboo.vmware.action;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapper;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapping;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHost;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHostManager;
import org.apache.log4j.Logger;

import java.util.Collection;

import static com.google.common.base.Optional.fromNullable;
import static org.apache.commons.lang.StringUtils.*;

public class MapAgentToVmAction extends BambooActionSupport implements GlobalAdminSecurityAware {

    private static final Logger log = Logger.getLogger(MapAgentToVmAction.class);

    private AgentToVmMapper agentToVmMapper;
    private AgentManager agentManager;
    private VHostManager vHostManager;

    private String mappingId;
    private String hostId;
    private String vmName;
    private Long agentId;
    private String snapshotName;
    private Boolean stopAgentAfterBuild;

    public MapAgentToVmAction(AgentToVmMapper agentToVmMapper, AgentManager agentManager, VHostManager vHostManager) {
        this.agentToVmMapper = agentToVmMapper;
        this.agentManager = agentManager;
        this.vHostManager = vHostManager;
    }

    @Override
    public String doDefault() throws Exception {
        if (hasErrors()) {
            return ERROR;
        }

        if (mappingId != null) {
            AgentToVmMapping mapping = agentToVmMapper.load(mappingId);
            setHostId(mapping.getVHostId());
            setVmName(mapping.getVmName());
            setAgentId(mapping.getAgentId());
            setSnapshotName(mapping.getSnapshotName().orNull());
            setStopAgentAfterBuild(mapping.isStopAgentAfterBuild());
        }

        return INPUT;
    }

    @Override
    public String execute() throws Exception {
        AgentToVmMapping mapping = new AgentToVmMapping(hostId, vmName, agentId, snapshotName, fromNullable(stopAgentAfterBuild).or(false));
        if (isNotBlank(mappingId)) {
            mapping.setId(mappingId);
        }

        mapping = agentToVmMapper.store(mapping);

        String operation = hostId != null ? "updated" : "created";
        log.warn("AgentToVmMapping " + operation + ":" + mapping);

        return SUCCESS;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String doDelete() throws Exception {
        if (isBlank(mappingId)) {
            return ERROR;
        }

        AgentToVmMapping mapping = agentToVmMapper.load(mappingId);

        if (mapping == null) {
            return ERROR;
        }

        agentToVmMapper.remove(mapping);
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (isBlank(hostId)) {
            addFieldError("hostId", getText("bvm.agent.mapping.vhost.empty"));
        }

        if (isBlank(vmName)) {
            addFieldError("vmName", getText("bvm.agent.mapping.vm.empty"));
        }

        if (agentId == null) {
            addFieldError("agentId", getText("bvm.agent.mapping.agent.empty"));
        }

        if (!isMappingEdit()) {
            if (!hasFieldErrors()) {
                Optional<AgentToVmMapping> mapping = getMappingToAgent(agentId);
                if (mapping.isPresent()) {
                    addFieldError("agentId", getText("bvm.agent.mapping.agent.already.mapped"));
                }
            }
        }
    }

    public Collection<VHost> getHosts() {
        return vHostManager.loadAll();
    }

    public Collection<BuildAgent> getAgents() {
        return agentManager.getAllRemoteAgents();
    }

    public BuildAgent getAgent() {
        BuildAgent agent = agentManager.getAgent(agentId);
        if (agent != null) {
            return agent;
        } else {
            throw new RuntimeException("Unable to find build agent with id " + agentId);
        }
    }

    public VHost getVHost() {
        VHost vHost = vHostManager.load(hostId);
        if (vHost != null) {
            return vHost;
        } else {
            throw new RuntimeException("Unable to find virtual host with id " + hostId);
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public boolean isMappingAction() {
        return true;
    }

    public String getMappingId() {
        return mappingId;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setMappingId(String mappingId) {
        this.mappingId = mappingId;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getVmName() {
        return vmName;
    }

    public void setVmName(String vmName) {
        this.vmName = vmName;
    }

    @SuppressWarnings("UnusedDeclaration")
    public Long getAgentId() {
        return agentId;
    }

    public void setAgentId(Long agentId) {
        this.agentId = agentId;
    }

    public String getSnapshotName() {
        return snapshotName;
    }

    public void setSnapshotName(String snapshotName) {
        this.snapshotName = snapshotName;
    }

    public Boolean getStopAgentAfterBuild() {
        return stopAgentAfterBuild;
    }

    public void setStopAgentAfterBuild(Boolean stopAgentAfterBuild) {
        this.stopAgentAfterBuild = stopAgentAfterBuild;
    }

    private Optional<AgentToVmMapping> getMappingToAgent(long agentId) {
        for (AgentToVmMapping mapping : agentToVmMapper.loadAll()) {
            if (mapping.getAgentId() == agentId) {
                return Optional.of(mapping);
            }
        }
        return Optional.absent();
    }

    private boolean isMappingEdit() {
        return isNotBlank(mappingId);
    }

}
