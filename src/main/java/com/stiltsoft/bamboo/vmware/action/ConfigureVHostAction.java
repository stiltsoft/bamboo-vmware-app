package com.stiltsoft.bamboo.vmware.action;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapper;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapping;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHost;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHostManager;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.apache.commons.lang.StringUtils.isEmpty;

public class ConfigureVHostAction extends BambooActionSupport implements GlobalAdminSecurityAware {

    private static final Logger log = Logger.getLogger(ConfigureVHostAction.class);

    private VHostManager vHostManager;
    private AgentToVmMapper agentToVmMapper;
    private AgentManager agentManager;

    private String hostId;
    private String hostUrl;
    private String hostUserName;
    private String hostPassword;

    public ConfigureVHostAction(VHostManager vHostManager,
                                AgentToVmMapper agentToVmMapper,
                                AgentManager agentManager) {

        this.vHostManager = vHostManager;
        this.agentToVmMapper = agentToVmMapper;
        this.agentManager = agentManager;
    }

    @Override
    public String doDefault() throws Exception {
        if (hasErrors()) {
            return ERROR;
        }

        if (hostId != null) {
            VHost vHost = vHostManager.load(hostId);
            setHostId(vHost.getId());
            setHostUrl(vHost.getUrl());
            setHostUserName(vHost.getUserName());
            setHostPassword(vHost.getPassword());
        }

        return INPUT;
    }

    @Override
    public String execute() throws Exception {
        VHost vHost = new VHost(hostUrl, hostUserName, hostPassword);
        if (!isEmpty(hostId)) {
            vHost.setId(hostId);
        }

        vHost = vHostManager.store(vHost);

        String operation = hostId != null ? "updated" : "created";
        log.warn("VHost " + operation + ":"  + vHost);

        return SUCCESS;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String doDelete() throws Exception {
        if (isEmpty(hostId)) {
            return ERROR;
        }

        VHost vHost = vHostManager.load(hostId);
        if (vHost == null) {
            return ERROR;
        }

        vHostManager.remove(vHost);
        return SUCCESS;
    }

    @Override
    public void validate() {
        if (isEmpty(hostUrl)) {
            addFieldError("hostUrl", getText("bvm.vhost.host.url.empty"));
        }

        if (isEmpty(hostUserName)) {
            addFieldError("hostUserName", getText("bvm.vhost.host.user.name.empty"));
        }

        if (isEmpty(hostPassword)) {
            addFieldError("hostPassword", getText("bvm.vhost.password.empty"));
        }
    }

    public Collection<BuildAgent> getMappedAgents() {
        List<BuildAgent> mappedAgents = new LinkedList<BuildAgent>();
        for (AgentToVmMapping mapping : agentToVmMapper.loadAll()) {
            if (mapping.getVHostId().equals(getHostId())) {
                BuildAgent agent = agentManager.getAgent(mapping.getAgentId());
                if (agent != null) {
                    mappedAgents.add(agent);
                }
            }
        }
        return mappedAgents;
    }

    public String getHostId() {
        return hostId;
    }

    public void setHostId(String hostId) {
        this.hostId = hostId;
    }

    public String getHostUrl() {
        return hostUrl;
    }

    public void setHostUrl(String hostUrl) {
        this.hostUrl = hostUrl;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String getHostUserName() {
        return hostUserName;
    }

    public void setHostUserName(String hostUserName) {
        this.hostUserName = hostUserName;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String getHostPassword() {
        return hostPassword;
    }

    public void setHostPassword(String hostPassword) {
        this.hostPassword = hostPassword;
    }

}
