package com.stiltsoft.bamboo.vmware.action;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.ww2.BambooActionSupport;
import com.atlassian.bamboo.ww2.aware.permissions.GlobalAdminSecurityAware;
import com.stiltsoft.bamboo.vmware.agentman.api.OnlineVMAgentOptimizer;
import com.stiltsoft.bamboo.vmware.manager.BvmSettingsManager;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapper;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapping;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHost;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHostManager;

import java.util.Collection;

public class VMWareMainConfigAction extends BambooActionSupport implements GlobalAdminSecurityAware {

    private VHostManager vHostManager;
    private AgentToVmMapper agentToVmMapper;
    private AgentManager agentManager;
    private OnlineVMAgentOptimizer onlineVMAgentOptimizer;
    private BvmSettingsManager bvmSettingsManager;

    private String enableButton, disableButton;

    @Override
    public String doDefault() throws Exception {
        return INPUT;
    }

    @SuppressWarnings("UnusedDeclaration")
    public String doReconfigure() throws Exception {
        final boolean enableClicked = enableButton != null;
        final boolean disableClicked = disableButton != null;

        if (enableClicked) {
            bvmSettingsManager.setAgentManagementEnabled(true);
        } else if (disableClicked) {
            bvmSettingsManager.setAgentManagementEnabled(false);
        } else {
            addActionError("No action specified.");
            return ERROR;
        }

        return SUCCESS;
    }

    public boolean isAgentManagementEnabled() {
        return bvmSettingsManager.isAgentManagementEnabled();
    }

    public int getAvailableAgentSlotCount() {
        return onlineVMAgentOptimizer.getAvailableAgentSlotCount();
    }

    public Collection<VHost> getVHosts() {
        return vHostManager.loadAll();
    }

    public Collection<AgentToVmMapping> getMappings() {
        return agentToVmMapper.loadAll();
    }

    public String getAgentName(long agentId) {
        BuildAgent agent = agentManager.getAgent(agentId);
        if (agent != null) {
            return agent.getName();
        } else {
            return "Unable to find agent with id " + agentId;
        }
    }

    public String getVHostData(String vHostId) {
        VHost vHost = vHostManager.load(vHostId);
        if (vHost != null) {
            return "<a href=" + vHost.getUrl() + ">" + vHost.getUrl() + "</a> (" + vHost.getUserName() + ")";
        } else {
            return "Unable to find virtual host with id " + vHostId;
        }
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setEnableButton(String enableButton) {
        this.enableButton = enableButton;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setDisableButton(String disableButton) {
        this.disableButton = disableButton;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setvHostManager(VHostManager vHostManager) {
        this.vHostManager = vHostManager;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setAgentToVmMapper(AgentToVmMapper agentToVmMapper) {
        this.agentToVmMapper = agentToVmMapper;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setAgentManager(AgentManager agentManager) {
        this.agentManager = agentManager;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setOnlineVMAgentOptimizer(OnlineVMAgentOptimizer onlineVMAgentOptimizer) {
        this.onlineVMAgentOptimizer = onlineVMAgentOptimizer;
    }

    @SuppressWarnings("UnusedDeclaration")
    public void setBvmSettingsManager(BvmSettingsManager bvmSettingsManager) {
        this.bvmSettingsManager = bvmSettingsManager;
    }

}
