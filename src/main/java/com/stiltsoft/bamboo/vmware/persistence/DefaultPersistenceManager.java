package com.stiltsoft.bamboo.vmware.persistence;

import com.atlassian.bandana.BandanaManager;

import static com.atlassian.bamboo.bandana.PlanAwareBandanaContext.GLOBAL_CONTEXT;

public class DefaultPersistenceManager implements PersistenceManager {

    private BandanaManager bandanaManager;

    public DefaultPersistenceManager(BandanaManager bandanaManager) {
        this.bandanaManager = bandanaManager;
    }

    @Override
    public void set(String key, Object value) {
        bandanaManager.setValue(GLOBAL_CONTEXT, key, value);
    }

    @Override
    public <T> T get(String key) {
        //noinspection unchecked
        return (T) bandanaManager.getValue(GLOBAL_CONTEXT, key);
    }

    @Override
    public void remove(String key) {
        bandanaManager.removeValue(GLOBAL_CONTEXT, key);
    }

    @Override
    public Iterable<String> getKeys() {
        return bandanaManager.getKeys(GLOBAL_CONTEXT);
    }

}
