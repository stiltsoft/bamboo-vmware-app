package com.stiltsoft.bamboo.vmware.persistence;

public interface PersistenceManager {

    void set(String key, Object value);

    <T> T get(String key);

    void remove(String key);

    Iterable<String> getKeys();

}
