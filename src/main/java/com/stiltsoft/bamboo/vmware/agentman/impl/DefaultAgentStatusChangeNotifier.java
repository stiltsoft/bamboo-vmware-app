package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentStatusChangeListener;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentStatusChangeNotifier;
import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTask;
import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTaskScheduler;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class DefaultAgentStatusChangeNotifier implements AgentStatusChangeNotifier {

    private PeriodicTaskScheduler periodicTaskScheduler;
    private AgentActivityCache agentActivityCache;
    private List<AgentStatusChangeListener> listeners = new CopyOnWriteArrayList<AgentStatusChangeListener>();

    public DefaultAgentStatusChangeNotifier(PeriodicTaskScheduler periodicTaskScheduler, AgentManager agentManager) {
        this.periodicTaskScheduler = periodicTaskScheduler;
        agentActivityCache = new AgentActivityCache(agentManager);
        init();
    }

    @Override
    public void addAgentStatusChangeListener(AgentStatusChangeListener agentStatusChangeListener) {
        listeners.add(agentStatusChangeListener);
    }

    private void init() {
        scheduleUpdateAgentDataTask();
    }

    private void scheduleUpdateAgentDataTask() {
        periodicTaskScheduler.schedule(new UpdateAgentDataTask());
    }

    private void notifyAgentsBecameOnline(Collection<BuildAgent> buildAgents) {
        for (AgentStatusChangeListener listener : listeners) {
            listener.onAgentsBecameOnline(buildAgents);
        }
    }

    private void updateCacheAndNotify() {
        List<BuildAgent> activatedAgents = agentActivityCache.updateAndGetActivated();
        if (!activatedAgents.isEmpty()) {
            notifyAgentsBecameOnline(activatedAgents);
        }
    }

    private class UpdateAgentDataTask implements PeriodicTask {

        @Override
        public void execute() {
            updateCacheAndNotify();
        }

        @Override
        public int getWeight() {
            return 20;
        }

    }

}
