package com.stiltsoft.bamboo.vmware.agentman.api;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;

import java.util.Collection;

public interface AgentStatusChangeListener {

    void onAgentsBecameOnline(Collection<BuildAgent> buildAgents);

}
