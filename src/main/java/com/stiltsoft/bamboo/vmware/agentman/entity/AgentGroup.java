package com.stiltsoft.bamboo.vmware.agentman.entity;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.LinkedList;

public class AgentGroup extends LinkedList<BuildAgent> implements Comparable<AgentGroup> {

    public AgentGroup() {
    }

    public AgentGroup(Collection<? extends BuildAgent> c) {
        super(c);
    }

    @Override
    public int compareTo(@NotNull AgentGroup o) {
        return o.size() - this.size();
    }

}
