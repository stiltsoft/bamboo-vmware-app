package com.stiltsoft.bamboo.vmware.agentman.api;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.base.Optional;

import java.util.Collection;
import java.util.Date;

public interface AgentController {

    public void enableAgent(BuildAgent agent);

    public void disableAgent(BuildAgent agent);

    public void makeAgentOffline(BuildAgent agent);

    public Collection<BuildAgent> getStartingAgents();

    public boolean isStartingAgent(BuildAgent agent);

    public void markAgentStarting(BuildAgent agent);

    public Collection<BuildAgent> getOnlineAgents();

    public Date getLastAgentActivityDate(long agentId);

    public void setLastAgentActivityDate(long agentId, Date date);

}
