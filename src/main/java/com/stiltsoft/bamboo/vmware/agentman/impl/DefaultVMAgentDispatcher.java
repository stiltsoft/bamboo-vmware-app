package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentController;
import com.stiltsoft.bamboo.vmware.agentman.api.OnlineVMAgentOptimizer;
import com.stiltsoft.bamboo.vmware.agentman.api.VMAgentDispatcher;
import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTask;
import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTaskScheduler;
import com.stiltsoft.bamboo.vmware.agentman.impl.timingexecutor.TimingVMAgentOperations;
import com.stiltsoft.bamboo.vmware.manager.BvmSettingsManager;
import org.apache.log4j.Logger;

import java.util.Collection;

public class DefaultVMAgentDispatcher implements VMAgentDispatcher {

    private static final Logger log = Logger.getLogger(DefaultVMAgentDispatcher.class);

    private PeriodicTaskScheduler periodicTaskScheduler;
    private OnlineVMAgentOptimizer onlineVMAgentOptimizer;
    private AgentController agentController;
    private TimingVMAgentOperations timingVMAgentOperations;
    private BvmSettingsManager bvmSettingsManager;

    public DefaultVMAgentDispatcher(PeriodicTaskScheduler periodicTaskScheduler,
                                    OnlineVMAgentOptimizer onlineVMAgentOptimizer,
                                    AgentController agentController,
                                    TimingVMAgentOperations timingVMAgentOperations,
                                    BvmSettingsManager bvmSettingsManager) {

        this.periodicTaskScheduler = periodicTaskScheduler;
        this.onlineVMAgentOptimizer = onlineVMAgentOptimizer;
        this.agentController = agentController;
        this.timingVMAgentOperations = timingVMAgentOperations;
        this.bvmSettingsManager = bvmSettingsManager;
        init();
    }

    @Override
    public void adjustOnlineAgents() {
        Collection<BuildAgent> vmAgentsToStart = onlineVMAgentOptimizer.getAgentsToStart();
        startAndEnableVMAgents(vmAgentsToStart);

        Collection<BuildAgent> vmAgentsToDisable = onlineVMAgentOptimizer.getAgentsToDisable();
        disableVMAgents(vmAgentsToDisable);

        Collection<BuildAgent> vmAgentsToStop = onlineVMAgentOptimizer.getAgentsToStop();
        shutdownVMAgents(vmAgentsToStop);
    }

    private void init() {
        scheduleAdjustOnlineAgentsTask();
    }

    private void scheduleAdjustOnlineAgentsTask() {
        periodicTaskScheduler.schedule(new AdjustOnlineAgentsTask());
    }

    private void startAndEnableVMAgents(Collection<BuildAgent> vmAgents) {
        for (BuildAgent agent : vmAgents) {
            timingVMAgentOperations.startVMAgent(agent);
        }
    }

    private void disableVMAgents(Collection<BuildAgent> vmAgents) {
        for (BuildAgent vmAgent : vmAgents) {
            agentController.disableAgent(vmAgent);
            log.warn("Agent '" + vmAgent.getName() + "' disabled.");
        }
    }

    private void shutdownVMAgents(Collection<BuildAgent> vmAgents) {
        for (BuildAgent agent : vmAgents) {
            timingVMAgentOperations.stopVMAgent(agent);
        }
    }

    private class AdjustOnlineAgentsTask implements PeriodicTask {

        @Override
        public void execute() {
            if (bvmSettingsManager.isAgentManagementEnabled()) {
                adjustOnlineAgents();
            }
        }

        @Override
        public int getWeight() {
            return 10;
        }

    }

}
