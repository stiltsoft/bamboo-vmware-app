package com.stiltsoft.bamboo.vmware.agentman.impl.scheduler;

import com.atlassian.bamboo.security.ImpersonationHelper;
import com.atlassian.bamboo.utils.BambooRunnables;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTask;
import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTaskScheduler;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;

import static java.lang.System.currentTimeMillis;

public class DefaultPeriodicTaskScheduler implements PeriodicTaskScheduler, InitializingBean, DisposableBean {

    private static final String INSTANCE_KEY = DefaultPeriodicTaskScheduler.class.getSimpleName();
    private static final String JOB_KEY = "vmManagePeriodicTasks";
    private static final long JOB_EXECUTION_INTERVAL_MS = 8000;

    private static final Logger log = Logger.getLogger(DefaultPeriodicTaskScheduler.class);

    private PluginScheduler pluginScheduler;
    private Set<PeriodicTask> tasks = new ConcurrentSkipListSet<PeriodicTask>(new TaskWeightComparator());

    public DefaultPeriodicTaskScheduler(PluginScheduler pluginScheduler) {
        this.pluginScheduler = pluginScheduler;
    }

    @Override
    public void schedule(PeriodicTask periodicTask) {
        tasks.add(periodicTask);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        log.warn("onStart() executed.");
        Map<String, Object> jobData = new HashMap<String, Object>();
        jobData.put(INSTANCE_KEY, this);
        pluginScheduler.scheduleJob(JOB_KEY, TaskExecutor.class, jobData, new Date(currentTimeMillis() + 20000), JOB_EXECUTION_INTERVAL_MS);
        log.warn("Scheduler job registered.");
    }

    @Override
    public void destroy() throws Exception {
        pluginScheduler.unscheduleJob(JOB_KEY);
        log.warn("Scheduler job unregistered.");
    }

    protected Set<PeriodicTask> getTasks() {
        return tasks;
    }

    public static class TaskExecutor implements PluginJob {

        public void execute(Map<String, Object> jobDataMap) {
            DefaultPeriodicTaskScheduler periodicTaskScheduler = getTaskScheduler(jobDataMap);

            for (final PeriodicTask periodicTask : periodicTaskScheduler.getTasks()) {
                try {
                    ImpersonationHelper.runWithSystemAuthority(new BambooRunnables.NotThrowing() {
                        @Override
                        public void run() {
                            periodicTask.execute();
                        }
                    });
                } catch (Exception e) {
                    log.error("Task execution failed. Task: " + periodicTask.getClass().getName(), e);
                }
            }
        }

        private DefaultPeriodicTaskScheduler getTaskScheduler(Map<String, Object> jobDataMap) {
            return (DefaultPeriodicTaskScheduler) jobDataMap.get(DefaultPeriodicTaskScheduler.INSTANCE_KEY);
        }

    }

}
