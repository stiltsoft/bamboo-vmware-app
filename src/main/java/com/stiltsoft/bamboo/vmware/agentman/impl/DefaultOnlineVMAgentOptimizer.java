package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.build.BuildExecutionManager;
import com.atlassian.bamboo.license.BambooLicenseManager;
import com.atlassian.bamboo.v2.build.agent.AgentIdleStatus;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentController;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentUsageAnalyzer;
import com.stiltsoft.bamboo.vmware.agentman.api.BuildAgentVMResolver;
import com.stiltsoft.bamboo.vmware.agentman.api.OnlineVMAgentOptimizer;
import com.stiltsoft.bamboo.vmware.agentman.entity.AgentGroup;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapping;
import org.apache.log4j.Logger;

import java.util.*;
import java.util.Map.Entry;

import static com.atlassian.bamboo.util.BambooDateUtils.getMillisDistanceToNow;
import static com.google.common.base.Optional.absent;
import static com.google.common.base.Optional.of;
import static java.util.Collections.sort;

public class DefaultOnlineVMAgentOptimizer implements OnlineVMAgentOptimizer {

    private static final Logger log = Logger.getLogger(DefaultOnlineVMAgentOptimizer.class);

    private static final long VM_AGENT_IDLE_THRESHOLD_MS = 60000;
    private static final long VM_AGENT_LAST_MODIFICATION_TIMEOUT_MS = 1000 * 60 * 3;

    private AgentUsageAnalyzer agentUsageAnalyzer;
    private BuildAgentVMResolver buildAgentVMResolver;
    private BuildExecutionManager buildExecutionManager;
    private AgentController agentController;
    private AgentCapabilitySetDescComparator agentCapabilitySetDescComparator;
    private BambooLicenseManager bambooLicenseManager;

    public DefaultOnlineVMAgentOptimizer(AgentUsageAnalyzer agentUsageAnalyzer,
                                         BuildAgentVMResolver buildAgentVMResolver,
                                         BuildExecutionManager buildExecutionManager,
                                         AgentController agentController,
                                         AgentCapabilitySetDescComparator agentCapabilitySetDescComparator,
                                         BambooLicenseManager bambooLicenseManager) {

        this.agentUsageAnalyzer = agentUsageAnalyzer;
        this.buildAgentVMResolver = buildAgentVMResolver;
        this.buildExecutionManager = buildExecutionManager;
        this.agentController = agentController;
        this.agentCapabilitySetDescComparator = agentCapabilitySetDescComparator;
        this.bambooLicenseManager = bambooLicenseManager;
    }

    @Override
    public Collection<BuildAgent> getAgentsToStart() {
        List<AgentGroup> requiredAgentGroups = agentUsageAnalyzer.getRequiredAgentGroups();

        sort(requiredAgentGroups);

        Set<BuildAgent> vmAgentsToStart = new HashSet<BuildAgent>();

        for (AgentGroup agentGroup : requiredAgentGroups) {
            if (getAvailableAgentSlotCount() <= 0) {
                log.warn("There are no available slots to start virtual agent.");
                break;
            }
            Optional<BuildAgent> agentToStartOpt = getVMAgentToStart(agentGroup);
            if (agentToStartOpt.isPresent()) {
                BuildAgent agent = agentToStartOpt.get();
                log.warn("VM agent '" + agent + "' must be started for build/deployment .");
                vmAgentsToStart.add(agent);
            }
        }

        return vmAgentsToStart;
    }

    @Override
    public Collection<BuildAgent> getAgentsToDisable() {
        return getAgentsToDisableOrStop(true);
    }

    @Override
    public Collection<BuildAgent> getAgentsToStop() {
        return getAgentsToDisableOrStop(false);
    }

    public int getAvailableAgentSlotCount() {
        int allowedAgents = bambooLicenseManager.getAllowedNumberOfRemoteAgents();
        int onlineAgents = agentController.getOnlineAgents().size();
        int startingAgents = agentController.getStartingAgents().size();

        int availableSlots = allowedAgents - (onlineAgents + startingAgents);
        if (availableSlots <= 0) {
            log.warn("Available virtual agent slot count: " + availableSlots +
                    ". LicenseAllowed: " + allowedAgents +
                    ". OnlineAgents: " + onlineAgents +
                    ". StartingAgents: " + startingAgents);
        }
        return availableSlots;
    }

    private List<BuildAgent> getAgentsToDisableOrStop(final boolean toDisable) {
        Set<BuildAgent> requiredAgents = agentUsageAnalyzer.getRequiredAgents();

        Map<BuildAgent, AgentToVmMapping> mappingByAgent = buildAgentVMResolver.getAllVMAgents();

        List<BuildAgent> resultList = new LinkedList<>();
        for (Entry<BuildAgent, AgentToVmMapping> entry : mappingByAgent.entrySet()) {
            BuildAgent buildAgent = entry.getKey();
            AgentToVmMapping mapping = entry.getValue();

            if (apply(buildAgent, mapping, toDisable, requiredAgents)) {
                resultList.add(buildAgent);
            }
        }

        return resultList;
    }

    private boolean hasNoBuildsRunning(final BuildAgent agent) {
        boolean idle = agent.getAgentStatus() instanceof AgentIdleStatus;
        boolean allAgentBuildsFinished = buildExecutionManager.getBuildRunningOnAgent(agent.getId()) == null;
        boolean noBuildsRunning = idle && allAgentBuildsFinished;
        if (!noBuildsRunning) {
            log.warn("Agent '" + agent.getName() + "' has running build.");
        }
        return noBuildsRunning;
    }


    private Optional<BuildAgent> getVMAgentToStart(List<BuildAgent> agents) {
        List<BuildAgent> vmAgentsToStart = new ArrayList<BuildAgent>();
        for (BuildAgent agent : agents) {
            if (agent.isActive() && agent.isEnabled()) {
                return absent();
            } else {
                if (buildAgentVMResolver.isVMAgent(agent)) {
                    if (agentController.isStartingAgent(agent)) {
                        return absent();
                    } else {
                        vmAgentsToStart.add(agent);
                    }
                }
            }
        }
        return findMostCapableAgent(vmAgentsToStart);
    }

    private boolean isIdleTooLong(BuildAgent agent, AgentToVmMapping mapping) {
        Date lastActivityDate = agentController.getLastAgentActivityDate(agent.getId());
        long idleTrashold = getVmAgentIdleThresholdMs(mapping);

        boolean idleLongerThan = getMillisDistanceToNow(lastActivityDate) >= idleTrashold;

        if (idleLongerThan) {
            log.warn("Agent '" + agent.getName() + "' is idle longer than " + idleTrashold + " ms.");
        }
        return idleLongerThan;
    }

    private long getVmAgentIdleThresholdMs(AgentToVmMapping mapping) {
        return mapping.isStopAgentAfterBuild() ? 5000 : VM_AGENT_IDLE_THRESHOLD_MS;
    }

    private Optional<BuildAgent> findMostCapableAgent(List<BuildAgent> candidates) {
        if (!candidates.isEmpty()) {
            sort(candidates, agentCapabilitySetDescComparator);
            return of(candidates.get(0));
        } else {
            return absent();
        }
    }

    private boolean apply(BuildAgent agent,
                          AgentToVmMapping mapping,
                          boolean toDisable,
                          Set<BuildAgent> requiredAgents) {

        long agentLastModificationDate = agent.getDefinition().getLastModificationDate().getTime();
        if (System.currentTimeMillis() - agentLastModificationDate < VM_AGENT_LAST_MODIFICATION_TIMEOUT_MS) {
            System.out.println("<<<3 min skip");
            return false;
        }

        boolean active = agent.isActive();
        boolean enabled = toDisable ? agent.isEnabled() : !agent.isEnabled();
        boolean notRequired = !requiredAgents.contains(agent);
        boolean hasNoBuildsRunning = !active || hasNoBuildsRunning(agent);
        boolean idleTooLong = !active || isIdleTooLong(agent, mapping);

        if (mapping.isStopAgentAfterBuild()) {
            notRequired = true;
        }

//        String methodName = toDisable ? "getAgentsToDisable" : "getAgentsToStop";
//        log.warn("Method: " + methodName +
//                "; Agent: " + agent.getName() +
//                "; active: " + active +
//                "; enabled: " + enabled +
//                "; notRequired: " + notRequired +
//                "; hasNoBuildsRunning: " + hasNoBuildsRunning +
//                "; idleTooLong: " + idleTooLong);

        return active &&
                enabled &&
                notRequired &&
                hasNoBuildsRunning &&
                idleTooLong;
    }

}
