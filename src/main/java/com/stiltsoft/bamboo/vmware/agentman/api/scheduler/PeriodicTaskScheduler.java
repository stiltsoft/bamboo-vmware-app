package com.stiltsoft.bamboo.vmware.agentman.api.scheduler;

public interface PeriodicTaskScheduler {

    void schedule(PeriodicTask periodicTask);

}
