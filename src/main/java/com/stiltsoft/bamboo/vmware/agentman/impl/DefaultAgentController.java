package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.buildqueue.PipelineDefinition;
import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.util.Narrow;
import com.atlassian.bamboo.v2.build.agent.AgentOfflineStatus;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.BuildAgentImpl;
import com.google.common.cache.*;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentController;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentStatusChangeListener;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentStatusChangeNotifier;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import static com.stiltsoft.bamboo.settings.BambooSettings.getHeartbeatTimeoutSeconds;
import static java.lang.System.currentTimeMillis;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.apache.commons.lang.time.DateUtils.MILLIS_PER_MINUTE;
import static org.apache.commons.lang.time.DateUtils.MILLIS_PER_SECOND;

public class DefaultAgentController implements AgentController, AgentStatusChangeListener {

    private static final Logger log = Logger.getLogger(DefaultAgentController.class);

    private static final long AGENT_START_TIMEOUT_MS = 3 * MILLIS_PER_MINUTE;

    private AgentManager agentManager;
    private LoadingCache<BuildAgent, Date> startingAgentsCache;
    private Map<Long, Date> lastActivityDateByAgentId = new ConcurrentHashMap<Long, Date>();

    public DefaultAgentController(AgentManager agentManager, AgentStatusChangeNotifier agentStatusChangeNotifier) {
        this.agentManager = agentManager;
        startingAgentsCache = createCache();
        agentStatusChangeNotifier.addAgentStatusChangeListener(this);
    }

    @Override
    public void enableAgent(BuildAgent agent) {
        if (!agent.isEnabled()) {
            setAgentEnabled(agent, true);
        }
    }

    @Override
    public void disableAgent(BuildAgent agent) {
        if (agent.isEnabled()) {
            setAgentEnabled(agent, false);
        }
    }

    @Override
    public void makeAgentOffline(BuildAgent agent) {
        BuildAgentImpl buildAgent = Narrow.downTo(agent, BuildAgentImpl.class);
        if (buildAgent != null) {
            buildAgent.setAgentStatus(AgentOfflineStatus.getInstance());
            long timeOut = currentTimeMillis() - getHeartbeatTimeoutSeconds() * MILLIS_PER_SECOND * 2;
            buildAgent.setLastUpdated(new Date(timeOut));
        }
        agent.setUnresponsive(true);
    }

    @Override
    public Collection<BuildAgent> getStartingAgents() {
        return startingAgentsCache.asMap().keySet();
    }

    @Override
    public boolean isStartingAgent(BuildAgent agent) {
        return startingAgentsCache.asMap().containsKey(agent);
    }

    @Override
    public void markAgentStarting(BuildAgent agent) {
        startingAgentsCache.getUnchecked(agent);
    }

    @Override
    public Collection<BuildAgent> getOnlineAgents() {
        return agentManager.getAllRemoteAgents(true);
    }

    @Override
    public Date getLastAgentActivityDate(long agentId) {
        Date lastActivityDate = lastActivityDateByAgentId.get(agentId);
        if (lastActivityDate == null) {
            lastActivityDate = new Date();
            lastActivityDateByAgentId.put(agentId, lastActivityDate);
        }
        return lastActivityDate;
    }

    @Override
    public void setLastAgentActivityDate(long agentId, Date date) {
        lastActivityDateByAgentId.put(agentId, date);
    }

    @Override
    public void onAgentsBecameOnline(Collection<BuildAgent> buildAgents) {
        for (BuildAgent agent : buildAgents) {
            log.warn("Agent '" + agent.getName() + "' is started.");
            startingAgentsCache.invalidate(agent);
        }
    }

    private void setAgentEnabled(BuildAgent agent, boolean enabled) {
        PipelineDefinition definition = agent.getDefinition();
        definition.setEnabled(enabled);
        agentManager.saveAnyPipeline(definition);
    }

    private LoadingCache<BuildAgent, Date> createCache() {
        return CacheBuilder.newBuilder()
                .concurrencyLevel(4)
                .maximumSize(100L)
                .expireAfterWrite(AGENT_START_TIMEOUT_MS, MILLISECONDS)
                .removalListener(new RemovalListener<BuildAgent, Date>() {
                    @Override
                    public void onRemoval(RemovalNotification<BuildAgent, Date> notification) {
                        log.warn("Agent '" + getAgentName(notification) + "' startup timeout expires.");
                    }
                })
                .build(new CacheLoader<BuildAgent, Date>() {
                    @Override
                    public Date load(BuildAgent key) throws Exception {
                        return new Date();
                    }
                });
    }

    private String getAgentName(RemovalNotification<BuildAgent, Date> notification) {
        return notification.getKey() != null ? notification.getKey().getName() : "Undefined";
    }

}
