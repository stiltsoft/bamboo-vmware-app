package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.deployments.execution.DeploymentContext;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.stiltsoft.bamboo.vmware.agentman.api.BuildActivityMonitor;
import org.apache.log4j.Logger;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class DefaultBuildActivityMonitor implements BuildActivityMonitor {

    private static final Logger log = Logger.getLogger(DefaultBuildActivityMonitor.class);

    private Map<String, BuildContext> buildByResultKey = new ConcurrentHashMap<String, BuildContext>();
    private Map<Long, DeploymentContext> deploymentByResultId = new ConcurrentHashMap<Long, DeploymentContext>();

    @Override
    public Collection<BuildContext> getActiveBuilds() {
        return buildByResultKey.values();
    }

    @Override
    public Collection<DeploymentContext> getActiveDeployments() {
        return deploymentByResultId.values();
    }

    @Override
    public void addActiveBuild(BuildContext buildContext) {
        buildByResultKey.put(buildContext.getBuildResultKey(), buildContext);
        log.warn("Build added: " + buildContext.getBuildResultKey());
    }

    @Override
    public boolean removeActiveBuild(BuildContext buildContext) {
        return removeActiveBuild(buildContext.getBuildResultKey());
    }

    @Override
    public boolean removeActiveBuild(String buildResultKey) {
        boolean removed = buildByResultKey.remove(buildResultKey) != null;
        if (removed) {
            log.warn("Build removed: " + buildResultKey);
        } else {
            log.error("Build not found: " + buildResultKey);
        }
        return removed;
    }

    @Override
    public void addActiveDeployment(DeploymentContext deploymentContext) {
        long resultId = deploymentContext.getDeploymentResultId();
        deploymentByResultId.put(resultId, deploymentContext);
        log.warn("Deployment added: " + resultId);
    }

    @Override
    public boolean removeActiveDeployment(long deploymentResultId) {
        boolean removed = deploymentByResultId.remove(deploymentResultId) != null;
        if (removed) {
            log.warn("Deployment removed: " + deploymentResultId);
        } else {
            log.error("Deployment not found. DeploymentResultId: " + deploymentResultId);
        }
        return removed;
    }

}
