package com.stiltsoft.bamboo.vmware.agentman.api;

import com.atlassian.bamboo.deployments.execution.DeploymentContext;
import com.atlassian.bamboo.v2.build.BuildContext;

import java.util.Collection;

public interface BuildActivityMonitor {

    Collection<BuildContext> getActiveBuilds();

    Collection<DeploymentContext> getActiveDeployments();

    void addActiveBuild(BuildContext buildContext);

    boolean removeActiveBuild(BuildContext buildContext);

    boolean removeActiveBuild(String buildResultKey);

    void addActiveDeployment(DeploymentContext deploymentContext);

    boolean removeActiveDeployment(long deploymentResultId);

}
