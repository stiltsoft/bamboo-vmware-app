package com.stiltsoft.bamboo.vmware.agentman.impl.timingexecutor;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.cache.*;
import org.apache.log4j.Logger;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class TimingVMAgentTaskExecutor {

    private static final Logger log = Logger.getLogger(TimingVMAgentTaskExecutor.class);

    private LoadingCache<BuildAgent, Date> taskCache;

    public TimingVMAgentTaskExecutor(CacheLoader<BuildAgent, Date> loader, long taskTimeoutMs) {
        taskCache = createCache(loader, taskTimeoutMs);
    }

    private LoadingCache<BuildAgent, Date> createCache(CacheLoader<BuildAgent, Date> loader, long taskTimeoutMs) {
        return CacheBuilder.newBuilder()
                .concurrencyLevel(4)
                .maximumSize(200)
                .expireAfterWrite(taskTimeoutMs, TimeUnit.MILLISECONDS)
                .removalListener(new RemovalListener<BuildAgent, Date>() {
                    @Override
                    public void onRemoval(RemovalNotification<BuildAgent, Date> notification) {
                        log.warn("VM startup expires: " + notification.getKey().getName());
                    }
                })
                .build(loader);
    }

    public Date execute(BuildAgent agent) throws Exception {
        return taskCache.get(agent);
    }

}
