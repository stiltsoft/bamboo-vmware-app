package com.stiltsoft.bamboo.vmware.agentman.impl.timingexecutor;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.base.Optional;
import com.google.common.cache.CacheLoader;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentController;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentHost;
import com.stiltsoft.bamboo.vmware.agentman.api.BuildAgentVMResolver;
import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.vmware.vim25.TaskInfoState;
import org.apache.log4j.Logger;

import java.util.Date;

import static com.vmware.vim25.TaskInfoState.success;

public class TimingVMAgentOperations {

    private static final Logger log = Logger.getLogger(TimingVMAgentOperations.class);

    private static final long START_AGENT_TIMEOUT_MS = 1000 * 60 * 5;
    private static final long STOP_AGENT_TIMEOUT_MS = 1000 * 60 * 5;

    private BuildAgentVMResolver buildAgentVMResolver;
    private AgentController agentController;

    private TimingVMAgentTaskExecutor startExecutor;
    private TimingVMAgentTaskExecutor stopExecutor;

    public TimingVMAgentOperations(BuildAgentVMResolver buildAgentVMResolver, AgentController agentController) {
        this.buildAgentVMResolver = buildAgentVMResolver;
        this.agentController = agentController;
        startExecutor = new TimingVMAgentTaskExecutor(createStartLoader(), START_AGENT_TIMEOUT_MS);
        stopExecutor = new TimingVMAgentTaskExecutor(createStopLoader(), STOP_AGENT_TIMEOUT_MS);
    }

    public void startVMAgent(BuildAgent agent) {
        try {
            startExecutor.execute(agent);
            agentController.markAgentStarting(agent);
        } catch (Exception e) {
            log.error("Unable to start vm and enable agent '" + agent.getName() + "'.", e);
        }
    }

    public void stopVMAgent(BuildAgent agent) {
        try {
            stopExecutor.execute(agent);
        } catch (Exception e) {
            log.error("Unable to stop vm agent '" + agent.getName() + "'.", e);
        }
    }

    private CacheLoader<BuildAgent, Date> createStartLoader() {
        return new CacheLoader<BuildAgent, Date>() {
            @Override
            public Date load(BuildAgent agent) throws Exception {
                AgentHost agentHost = buildAgentVMResolver.resolveVM(agent);
                VMachine vm = agentHost.getVm();
                Optional<String> vmStartUpSnapshotNameOpt = agentHost.getStartUpSnapshotNameOpt();

                if (vmStartUpSnapshotNameOpt.isPresent()) {
                    TaskInfoState revertTaskState = vm.revertToSnapshot(vmStartUpSnapshotNameOpt.get());
                    if (revertTaskState != success) {
                        log.warn("Couldn't revert agent vm to snapshot. TaskState: " + revertTaskState);
                    }
                }

                TaskInfoState powerOnTaskState = vm.checkedPowerOn();
                if (powerOnTaskState != success) {
                    log.warn("Couldn't start agent vm. TaskState: " + powerOnTaskState);
                }

                agentController.enableAgent(agent);
                log.warn("Agent VM '" + vm.getName() + "' started. Agent '" + agent + "' enabled.");
                return new Date();
            }
        };
    }

    private CacheLoader<BuildAgent, Date> createStopLoader() {
        return new CacheLoader<BuildAgent, Date>() {
            @Override
            public Date load(BuildAgent agent) throws Exception {
                VMachine vm = buildAgentVMResolver.resolveVM(agent).getVm();
                vm.shutdownGuest();
                agentController.makeAgentOffline(agent);
                log.warn("Agent VM '" + vm.getName() + "' stopped. Agent '" + agent.getName() + "'.");
                return new Date();
            }
        };
    }

}
