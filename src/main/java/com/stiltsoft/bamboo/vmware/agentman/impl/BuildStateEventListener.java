package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.deployments.events.DeploymentTriggeredEvent;
import com.atlassian.bamboo.deployments.execution.events.DeploymentFinishedEvent;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.deployments.results.service.DeploymentResultService;
import com.atlassian.bamboo.event.BuildCanceledEvent;
import com.atlassian.bamboo.plan.PlanResultKey;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.resultsummary.ResultsSummaryManager;
import com.atlassian.bamboo.v2.build.events.BuildTriggeredEvent;
import com.atlassian.bamboo.v2.build.events.PostBuildCompletedEvent;
import com.atlassian.bamboo.v2.build.timing.BuildTimingPoints;
import com.atlassian.event.api.EventListener;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentController;
import com.stiltsoft.bamboo.vmware.agentman.api.BuildActivityMonitor;

import java.util.Date;

import static com.atlassian.bamboo.deployments.execution.events.DeploymentTimingPoints.ExecutionCancelled;

public class BuildStateEventListener {

    private BuildActivityMonitor buildActivityMonitor;
    private AgentController agentController;
    private DeploymentResultService deploymentResultService;
    private ResultsSummaryManager resultsSummaryManager;

    public BuildStateEventListener(BuildActivityMonitor buildActivityMonitor,
                                   AgentController agentController,
                                   DeploymentResultService deploymentResultService,
                                   ResultsSummaryManager resultsSummaryManager) {

        this.buildActivityMonitor = buildActivityMonitor;
        this.agentController = agentController;
        this.deploymentResultService = deploymentResultService;
        this.resultsSummaryManager = resultsSummaryManager;
    }

    @EventListener
    public void onBuildTriggeredEvent(BuildTriggeredEvent buildTriggeredEvent) {
        buildActivityMonitor.addActiveBuild(buildTriggeredEvent.getContext());
    }

    @EventListener
    public void onPostBuildCompletedEvent(PostBuildCompletedEvent postBuildCompletedEvent) {
        buildActivityMonitor.removeActiveBuild(postBuildCompletedEvent.getContext());
        PlanResultKey planResultKey = postBuildCompletedEvent.getContext().getPlanResultKey();
        ResultsSummary resultsSummary = resultsSummaryManager.getResultsSummary(planResultKey);
        if (resultsSummary != null && resultsSummary.getBuildAgentId() != null) {
            Long agentId = resultsSummary.getBuildAgentId();
            agentController.setLastAgentActivityDate(agentId, new Date());
        }
    }

    @EventListener
    public void onBuildCanceledEvent(BuildCanceledEvent buildCanceledEvent) {
        buildActivityMonitor.removeActiveBuild(buildCanceledEvent.getBuildResultKey());
    }

    @EventListener
    public void onDeploymentTriggered(DeploymentTriggeredEvent deploymentTriggeredEvent) {
        buildActivityMonitor.addActiveDeployment(deploymentTriggeredEvent.getContext());
    }

    @EventListener
    public void onDeploymentFinishedEvent(DeploymentFinishedEvent deploymentFinished) {
        long deploymentResultId = deploymentFinished.getDeploymentResultId();
        buildActivityMonitor.removeActiveDeployment(deploymentResultId);

        DeploymentResult deploymentResult = deploymentResultService.getDeploymentResult(deploymentResultId);
        if (deploymentResult != null && deploymentResult.getAgent() != null) {
            agentController.setLastAgentActivityDate(deploymentResult.getAgent().getId(), new Date());
        }
    }

    /**
     * @see BuildStateEventListener#onDeploymentFinishedEvent(com.atlassian.bamboo.deployments.execution.events.DeploymentFinishedEvent)
     */
    @EventListener
    public void onDeploymentExecutionCancelled(ExecutionCancelled deploymentCancelled) {
        throw new RuntimeException("com.stiltsoft.bamboo.vmware.agentman.impl.BuildStateEventListener.onDeploymentExecutionCancelled");
    }

    @EventListener
    public void onChainStarted(BuildTimingPoints.AgentAssigned agentAssigned) {
//        System.out.println("Busy agents");
//        System.out.println(agentManager.getBusyBuildAgents());

//        BuildAgent agent = agentManager.getAgent(agentAssigned.getAgentId());
//        Optional<AgentToVmMapping> mappingOpt = buildAgentVMResolver.resolveMapping(agent);
//        if (mappingOpt.isPresent() && mappingOpt.get().isStopAgentAfterBuild()) {
//            agentController.disableAgent(agent);
//            System.out.println("Disabled agent: " + agent);
//        }

//        System.out.println(agentAssigned.getPlanResultKey());
//        System.out.println(agentAssigned.getAgentId());
    }

}
