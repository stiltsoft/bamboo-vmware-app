package com.stiltsoft.bamboo.vmware.agentman.api;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapping;

import java.util.Map;

public interface BuildAgentVMResolver {

    AgentHost resolveVM(BuildAgent buildAgent);

    Optional<AgentToVmMapping> resolveMapping(BuildAgent buildAgent);

    boolean isVMAgent(BuildAgent buildAgent);

    Map<BuildAgent, AgentToVmMapping> getAllVMAgents();

}