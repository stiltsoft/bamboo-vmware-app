package com.stiltsoft.bamboo.vmware.agentman.impl.scheduler;

import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTask;

import java.util.Comparator;

public class TaskWeightComparator implements Comparator<PeriodicTask> {

    @Override
    public int compare(PeriodicTask o1, PeriodicTask o2) {
        return o1.getWeight() - o2.getWeight();
    }

}