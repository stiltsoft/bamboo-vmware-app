package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.deployments.environments.service.EnvironmentService;
import com.atlassian.bamboo.deployments.execution.DeploymentContext;
import com.atlassian.bamboo.plan.ExecutableAgentsHelper;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.cache.CachedPlanManager;
import com.atlassian.bamboo.plan.cache.ImmutableBuildable;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.RequirementSet;
import com.google.common.base.Function;
import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentUsageAnalyzer;
import com.stiltsoft.bamboo.vmware.agentman.api.BuildActivityMonitor;
import com.stiltsoft.bamboo.vmware.agentman.entity.AgentGroup;
import org.apache.log4j.Logger;

import java.util.*;

import static com.atlassian.bamboo.buildqueue.manager.AgentAssignmentService.AgentAssignmentExecutable;
import static com.atlassian.bamboo.buildqueue.manager.AgentAssignmentServiceHelper.asExecutables;
import static com.atlassian.bamboo.buildqueue.manager.AgentAssignmentServiceHelper.environmentToExecutables;
import static com.atlassian.bamboo.plan.ExecutableAgentsHelper.ExecutorQuery.newQueryWithoutAssignments;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.notNull;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.transform;
import static com.google.common.collect.Lists.newLinkedList;
import static java.util.Collections.emptyList;

public class DefaultAgentUsageAnalyzer implements AgentUsageAnalyzer {

    private static final Logger log = Logger.getLogger(DefaultAgentUsageAnalyzer.class);

    private BuildActivityMonitor buildActivityMonitor;
    private CachedPlanManager cachedPlanManager;
    private EnvironmentService environmentService;
    private ExecutableAgentsHelper executableAgentsHelper;

    public DefaultAgentUsageAnalyzer(BuildActivityMonitor buildActivityMonitor,
                                     CachedPlanManager cachedPlanManager,
                                     EnvironmentService environmentService,
                                     ExecutableAgentsHelper executableAgentsHelper) {

        this.buildActivityMonitor = buildActivityMonitor;
        this.cachedPlanManager = cachedPlanManager;
        this.environmentService = environmentService;
        this.executableAgentsHelper = executableAgentsHelper;
    }

    @Override
    public Set<BuildAgent> getRequiredAgents() {
        Set<BuildAgent> requiredAgents = new HashSet<BuildAgent>();

        for (AgentGroup group : getRequiredAgentGroups()) {
            for (BuildAgent agent : group) {
                requiredAgents.add(agent);
            }
        }

        return requiredAgents;
    }

    @Override
    public List<AgentGroup> getRequiredAgentGroups() {
        List<AgentGroup> agentGroups = new LinkedList<AgentGroup>();

        Collection<AgentGroup> agentGroupsForBuilds = getRequiredAgentGroupsForBuilds(getBuildables());
        Collection<AgentGroup> agentGroupsForDeployments =
                getRequiredAgentGroupsForDeployments(buildActivityMonitor.getActiveDeployments());

        agentGroups.addAll(agentGroupsForBuilds);
        agentGroups.addAll(agentGroupsForDeployments);

        return agentGroups;
    }


    private List<ImmutableBuildable> getBuildables(final Collection<BuildContext> buildQueue) {
        Iterable<ImmutableBuildable> buildables =
                transform(buildQueue, new Function<BuildContext, ImmutableBuildable>() {
                    @Override
                    public ImmutableBuildable apply(BuildContext buildContext) {
                        PlanKey planKey = checkNotNull(buildContext).getPlanResultKey().getPlanKey();
                        ImmutableBuildable planByResultKey = cachedPlanManager.getPlanByKey(planKey, ImmutableBuildable.class);
                        if (planByResultKey == null) {
                            log.info("Skipping " + planKey + ", not found in the database");
                        }
                        return planByResultKey;
                    }
                });
        return newLinkedList(filter(buildables, notNull()));
    }

    private List<ImmutableBuildable> getBuildables() {
        Collection<BuildContext> buildQueue = buildActivityMonitor.getActiveBuilds();
        return getBuildables(buildQueue);
    }

    private Collection<AgentGroup> getRequiredAgentGroupsForBuilds(Collection<ImmutableBuildable> buildables) {
        List<AgentGroup> agentGroups = new LinkedList<AgentGroup>();

        for (ImmutableBuildable buildable : buildables) {
            Collection<BuildAgent> compatibleAgents = getCompatibleAgents(buildable);
            agentGroups.add(new AgentGroup(compatibleAgents));
        }

        return agentGroups;
    }

    private Collection<AgentGroup> getRequiredAgentGroupsForDeployments(Collection<DeploymentContext> contexts) {
        List<AgentGroup> agentGroups = new LinkedList<AgentGroup>();

        for (DeploymentContext context : contexts) {
            Collection<BuildAgent> compatibleAgents = getCompatibleAgents(context);
            agentGroups.add(new AgentGroup(compatibleAgents));
        }

        return agentGroups;
    }

    private Collection<BuildAgent> getCompatibleAgents(ImmutableBuildable buildable) {
        RequirementSet requirements = buildable.getEffectiveRequirementSet();
        return queryForAgents(requirements, asExecutables(buildable), Optional.<CommonContext>absent());
    }

    private Collection<BuildAgent> getCompatibleAgents(DeploymentContext deploymentContext) {
        long environmentId = deploymentContext.getEnvironmentId();
        RequirementSet requirements = environmentService.getEnvironmentRequirementSet(environmentId);
        if (requirements == null) {
            log.warn("Asked to queue environment with id " + environmentId +
                    " but no environment with this key was found.  Aborting queue request");
            return emptyList();
        }
        Iterable<AgentAssignmentExecutable> executables = environmentToExecutables(environmentId);
        return queryForAgents(requirements, executables, Optional.<CommonContext>of(deploymentContext));
    }

    private Collection<BuildAgent> queryForAgents(RequirementSet requirements,
                                                  Iterable<AgentAssignmentExecutable> executables,
                                                  Optional<CommonContext> contextOption) {

        ExecutableAgentsHelper.ExecutorQuery executorQuery = newQueryWithoutAssignments(requirements)
                .withAssignmentsFor(executables)
                .withDisabledIncluded()
                .withOfflineIncluded();

        if (contextOption.isPresent()) {
            executorQuery.withContext(contextOption.get());
        }

        return executableAgentsHelper.getExecutableAgents(executorQuery);
    }

}
