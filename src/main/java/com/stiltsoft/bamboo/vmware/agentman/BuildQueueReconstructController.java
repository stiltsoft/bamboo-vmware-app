package com.stiltsoft.bamboo.vmware.agentman;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.queue.BuildQueueManager;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentStatusChangeListener;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentStatusChangeNotifier;
import org.apache.log4j.Logger;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class BuildQueueReconstructController implements AgentStatusChangeListener {

    private static final Logger log = Logger.getLogger(BuildQueueReconstructController.class);

    private BuildQueueManager buildQueueManager;

    public BuildQueueReconstructController(BuildQueueManager buildQueueManager,
                                           AgentStatusChangeNotifier agentStatusChangeNotifier) {

        this.buildQueueManager = buildQueueManager;
        agentStatusChangeNotifier.addAgentStatusChangeListener(this);
    }

    @Override
    public void onAgentsBecameOnline(Collection<BuildAgent> buildAgents) {
        log.warn("Activated agents: " + getAgentNames(buildAgents));
        reconstructBuildQueue();
    }

    private List<String> getAgentNames(Collection<BuildAgent> buildAgents) {
        List<String> agentNames = new ArrayList<String>();
        for (BuildAgent buildAgent : buildAgents) {
            agentNames.add(buildAgent.getName());
        }
        return agentNames;
    }

    private void reconstructBuildQueue() {
        try {
            Method method = buildQueueManager.getClass().getMethod("reconstructBuildQueue");

            if (method != null) {
                method.invoke(buildQueueManager);
            }
        } catch (NoSuchMethodException ignore) {
        } catch (Exception e) {
            throw new RuntimeException("Unable to reconstruct build queue.", e);
        }
    }

}
