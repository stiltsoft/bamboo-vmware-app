package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.atlassian.bamboo.v2.build.agent.capability.CapabilitySetManager;
import com.atlassian.bamboo.v2.build.agent.capability.ReadOnlyCapabilitySet;

import java.util.Comparator;

public class AgentCapabilitySetDescComparator implements Comparator<BuildAgent> {

    private CapabilitySetManager capabilitySetManager;

    public AgentCapabilitySetDescComparator(CapabilitySetManager capabilitySetManager) {
        this.capabilitySetManager = capabilitySetManager;
    }

    @Override
    public int compare(BuildAgent o1, BuildAgent o2) {
        int capabilitySize1 = getAgentCapabilitySize(o1.getId());
        int capabilitySize2 = getAgentCapabilitySize(o2.getId());
        return Integer.valueOf(capabilitySize2).compareTo(capabilitySize1);
    }

    private int getAgentCapabilitySize(long agentId) {
        ReadOnlyCapabilitySet capabilitySet = capabilitySetManager.getCombinedCapabilitySet(agentId);
        return capabilitySet != null ? capabilitySet.getCapabilities().size() : 0;
    }

}
