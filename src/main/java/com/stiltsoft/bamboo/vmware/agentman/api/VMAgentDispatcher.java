package com.stiltsoft.bamboo.vmware.agentman.api;

public interface VMAgentDispatcher {

    void adjustOnlineAgents();

}
