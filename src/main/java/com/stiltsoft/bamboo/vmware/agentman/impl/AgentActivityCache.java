package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class AgentActivityCache {

    private AgentManager agentManager;
    private Map<Long, Boolean> agentActivityById = new HashMap<Long, Boolean>();

    public AgentActivityCache(AgentManager agentManager) {
        this.agentManager = agentManager;
        init();
    }

    private void init() {
        for (BuildAgent buildAgent : agentManager.getAllRemoteAgents()) {
            agentActivityById.put(buildAgent.getId(), buildAgent.isActive());
        }
    }

    public List<BuildAgent> updateAndGetActivated() {
        List<BuildAgent> updatedAgents = new LinkedList<BuildAgent>();
        for (BuildAgent buildAgent : agentManager.getAllRemoteAgents()) {
            Boolean previousStateActive = agentActivityById.get(buildAgent.getId());
            Boolean currentStateActive = buildAgent.isActive();
            if (!currentStateActive.equals(previousStateActive)) {
                agentActivityById.put(buildAgent.getId(), currentStateActive);
                if (previousStateActive != null && currentStateActive) {
                    updatedAgents.add(buildAgent);
                }
            }
        }
        return updatedAgents;
    }

}
