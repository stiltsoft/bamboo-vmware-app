package com.stiltsoft.bamboo.vmware.agentman.api;

import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.service.VMachine;

public class AgentHost {

    private VMachine vm;

    private Optional<String> startUpSnapshotNameOpt;

    public AgentHost(VMachine vm, Optional<String> startUpSnapshotNameOpt) {
        this.vm = vm;
        this.startUpSnapshotNameOpt = startUpSnapshotNameOpt;
    }

    public VMachine getVm() {
        return vm;
    }

    public Optional<String> getStartUpSnapshotNameOpt() {
        return startUpSnapshotNameOpt;
    }

}
