package com.stiltsoft.bamboo.vmware.agentman.api.scheduler;

public interface PeriodicTask {

    void execute();

    int getWeight();

}
