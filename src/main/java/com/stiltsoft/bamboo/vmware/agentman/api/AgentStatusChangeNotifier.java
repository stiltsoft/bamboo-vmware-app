package com.stiltsoft.bamboo.vmware.agentman.api;

public interface AgentStatusChangeNotifier {

    void addAgentStatusChangeListener(AgentStatusChangeListener agentStatusChangeListener);

}
