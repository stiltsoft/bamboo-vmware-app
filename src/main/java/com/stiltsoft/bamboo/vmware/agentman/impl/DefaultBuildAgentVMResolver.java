package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.agentman.api.AgentHost;
import com.stiltsoft.bamboo.vmware.agentman.api.BuildAgentVMResolver;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapper;
import com.stiltsoft.bamboo.vmware.manager.agent2vm.AgentToVmMapping;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHost;
import com.stiltsoft.bamboo.vmware.manager.vhost.VHostManager;
import com.stiltsoft.bamboo.vmware.service.DefaultVMwareService;
import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.stiltsoft.bamboo.vmware.service.VMwareService;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Optional.absent;
import static com.google.common.base.Optional.of;

public class DefaultBuildAgentVMResolver implements BuildAgentVMResolver {

    private AgentManager agentManager;
    private VHostManager vHostManager;
    private AgentToVmMapper agentToVmMapper;

    public DefaultBuildAgentVMResolver(AgentManager agentManager,
                                       VHostManager vHostManager,
                                       AgentToVmMapper agentToVmMapper) {

        this.agentManager = agentManager;
        this.vHostManager = vHostManager;
        this.agentToVmMapper = agentToVmMapper;
    }

    @Override
    public AgentHost resolveVM(BuildAgent agent) {
        if (isVMAgent(agent)) {
            return getVM(agent);
        } else {
            return null;
        }
    }

    @Override
    public Optional<AgentToVmMapping> resolveMapping(BuildAgent buildAgent) {
        long agentId = buildAgent.getId();
        for (AgentToVmMapping mapping : agentToVmMapper.loadAll()) {
            if (mapping.getAgentId() == agentId) {
                return of(mapping);
            }
        }
        return absent();
    }

    @Override
    public boolean isVMAgent(BuildAgent buildAgent) {
        long agentId = buildAgent.getId();
        for (AgentToVmMapping mapping : agentToVmMapper.loadAll()) {
            if (mapping.getAgentId() == agentId) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Map<BuildAgent, AgentToVmMapping> getAllVMAgents() {
        Map<Long, AgentToVmMapping> mappingByBuildAgentId = new HashMap<>();
        for (AgentToVmMapping mapping : agentToVmMapper.loadAll()) {
            mappingByBuildAgentId.put(mapping.getAgentId(), mapping);
        }

        Map<BuildAgent, AgentToVmMapping> mappingByAgent = new HashMap<>();
        for (BuildAgent buildAgent : agentManager.getAllRemoteAgents()) {
            Long buildAgentId = buildAgent.getId();
            if (mappingByBuildAgentId.containsKey(buildAgentId)) {
                mappingByAgent.put(buildAgent, mappingByBuildAgentId.get(buildAgentId));
            }
        }

        return mappingByAgent;
    }

    private AgentHost getVM(BuildAgent agent) {
        AgentToVmMapping mapping = getMapping(agent);
        VHost vHost = vHostManager.load(mapping.getVHostId());
        VMachine vMachine = getVMWareService(vHost).getVirtualMachine(mapping.getVmName());
        return new AgentHost(vMachine, mapping.getSnapshotName());
    }

    private AgentToVmMapping getMapping(BuildAgent agent) {
        for (AgentToVmMapping mapping : agentToVmMapper.loadAll()) {
            if (mapping.getAgentId() == agent.getId()) {
                return mapping;
            }
        }
        throw new RuntimeException("Unable to find agent to vm mapping for agent " + agent.getName());
    }

    private VMwareService getVMWareService(VHost vHost) {
        try {
            return new DefaultVMwareService(vHost);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Unable to connect to virtual host: " + vHost);
        } catch (RemoteException e) {
            throw new RuntimeException("Unable to connect to virtual host: " + vHost);
        }
    }

}
