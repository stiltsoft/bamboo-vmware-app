package com.stiltsoft.bamboo.vmware.agentman.api;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import com.stiltsoft.bamboo.vmware.agentman.entity.AgentGroup;

import java.util.List;
import java.util.Set;

public interface AgentUsageAnalyzer {

    Set<BuildAgent> getRequiredAgents();

    List<AgentGroup> getRequiredAgentGroups();

}
