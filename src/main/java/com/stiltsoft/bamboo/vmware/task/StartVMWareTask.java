package com.stiltsoft.bamboo.vmware.task;

import com.google.common.base.Optional;
import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.stiltsoft.bamboo.vmware.service.VMachineOperationException;
import com.stiltsoft.bamboo.vmware.task.param.ActionParamHolder;
import com.vmware.vim25.TaskInfoState;

import static com.stiltsoft.bamboo.vmware.task.param.StartVmStateOption.SNAPSHOT;
import static com.vmware.vim25.TaskInfoState.success;

public class StartVMWareTask extends VMWareTask {

    @Override
    public TaskInfoState doAction(VMachine vmachine, ActionParamHolder paramHolder)
            throws VMachineOperationException, VMwareActionException {

        if (paramHolder.getStartVmStateOption() == SNAPSHOT) {
            TaskInfoState taskInfoState = vmachine.revertToSnapshot(paramHolder.getSnapshotName());
            if (taskInfoState != success) {
                throw new VMwareActionException("Couldn't revert virtual machine to snapshot. TaskInfoState: " +
                        taskInfoState);
            }
        }

        TaskInfoState taskInfoState = startVm(vmachine);
        if (taskInfoState != success) {
            throw new VMwareActionException("Couldn't start virtual machine. TaskInfoState: " + taskInfoState);
        }

        Optional<Long> guestStartTimeoutMsOption = paramHolder.getGuestStartTimeoutMs();
        if (guestStartTimeoutMsOption.isPresent()) {
            long guestStartTimeoutMs = guestStartTimeoutMsOption.get();

            VMachine.WaitForStartResult result = vmachine.waitForGuestToStart(guestStartTimeoutMs);

            if (!result.isVmWareToolsRunning()) {
                throw new VMwareActionException("VMware tools is not starting in " + guestStartTimeoutMs + " ms.");
            }

            if (!result.isHostNameDetected()) {
                throw new VMwareActionException("Guest host name is not detected in" + guestStartTimeoutMs + " ms.");
            }
        }

        return taskInfoState;
    }

    private TaskInfoState startVm(VMachine vmachine) throws VMachineOperationException {
        if (vmachine.isPoweredOn()) {
            addBuildLogEntry("Virtual machine '" + vmachine.getName() + "' has been already started");
            return success;
        } else {
            return vmachine.powerOn();
        }
    }

}
