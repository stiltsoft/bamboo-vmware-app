package com.stiltsoft.bamboo.vmware.task.param;

public enum StartVmStateOption {

    CURRENT,
    SNAPSHOT

}
