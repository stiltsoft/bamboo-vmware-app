package com.stiltsoft.bamboo.vmware.task;

import com.atlassian.bamboo.build.CustomPostBuildCompletedAction;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import com.atlassian.bamboo.v2.build.BuildContext;
import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.stiltsoft.bamboo.vmware.task.param.ActionParamHolder;
import com.stiltsoft.bamboo.vmware.task.param.PostBuildActionOption;
import com.vmware.vim25.TaskInfoState;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

import static com.atlassian.bamboo.task.TaskState.SUCCESS;
import static com.stiltsoft.bamboo.vmware.task.param.PostBuildActionOption.DO_NOTHING;
import static com.vmware.vim25.TaskInfoState.success;

public class VMWarePostBuildCompleteAction extends VMwareAction implements CustomPostBuildCompletedAction {

    private static final String VMWARE_START_TASK_KEY = "com.stiltsoft.bamboo.bamboo-vmware-plugin:vmwareStartTask";

    private BuildContext buildContext;

    @Override
    public void init(@NotNull BuildContext buildContext) {
        this.buildContext = buildContext;
    }

    @NotNull
    @Override
    public BuildContext call() throws VMwareActionException {
        for (TaskDefinition taskDef : getVMwareStartTaskDefinitions()) {
            ActionParamHolder paramHolder = new ActionParamHolder(taskDef.getConfiguration());

            if (isExecutionAllowed(paramHolder, taskDef.getId())) {
                execute(paramHolder);
            }
        }
        return buildContext;
    }

    @Override
    public TaskInfoState doAction(VMachine vmachine, ActionParamHolder paramHolder) throws VMwareActionException {
        PostBuildActionOption option = paramHolder.getPostBuildCompleteOption();
        switch (option) {
            case STOP_VM:
                return vmachine.checkedPowerOff();
            case SUSPEND_VM:
                return vmachine.checkedSuspend();
            case SHUT_DOWN_GUEST:
                vmachine.shutdownGuest();
                return success;
            default:
                throw new RuntimeException("Unsupported PostBuildCompleteAction " + option);
        }
    }

    private boolean isExecutionAllowed(ActionParamHolder paramHolder, long startTaskId) {
        return paramHolder.getPostBuildCompleteOption() != DO_NOTHING && getStartTaskState(startTaskId) == SUCCESS;
    }

    private List<TaskDefinition> getVMwareStartTaskDefinitions() {
        List<TaskDefinition> startTaskDefinitions = new ArrayList<TaskDefinition>();
        for (TaskDefinition taskDef : buildContext.getBuildDefinition().getTaskDefinitions()) {
            if (VMWARE_START_TASK_KEY.equals(taskDef.getPluginKey())) {
                startTaskDefinitions.add(taskDef);
            }
        }
        return startTaskDefinitions;
    }

    @Nullable
    private TaskState getStartTaskState(long taskId) {
        for (TaskResult taskResult : buildContext.getBuildResult().getTaskResultsForTask(VMWARE_START_TASK_KEY)) {
            if (taskId == taskResult.getTaskIdentifier().getId()) {
                return taskResult.getTaskState();
            }
        }
        return null;
    }

}
