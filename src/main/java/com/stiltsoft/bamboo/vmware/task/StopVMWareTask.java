package com.stiltsoft.bamboo.vmware.task;

import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.stiltsoft.bamboo.vmware.task.param.ActionParamHolder;
import com.vmware.vim25.TaskInfoState;

import static com.vmware.vim25.TaskInfoState.success;

public class StopVMWareTask extends VMWareTask {

    @Override
    public TaskInfoState doAction(VMachine vmachine, ActionParamHolder paramHolder) throws VMwareActionException {
        switch (paramHolder.getStopTaskActionOption()) {
            case STOP_VM:
                return powerOffVm(vmachine);
            case SUSPEND_VM:
                return suspendVm(vmachine);
            case SHUT_DOWN_GUEST:
                return shutDownGuest(vmachine);
            default:
                throw new IllegalArgumentException("Unsupported option: " + paramHolder.getStopTaskActionOption());
        }
    }

    private TaskInfoState shutDownGuest(VMachine vmachine) throws VMwareActionException {
        if (vmachine.isPoweredOff()) {
            addBuildLogEntry("Virtual machine '" + vmachine.getName() + "' has been already stopped");
            return success;
        } else if (vmachine.isSuspended()) {
            addBuildLogEntry("Can't stop virtual machine '" + vmachine.getName() + "' because it is suspended.");
            return success;
        } else {
            vmachine.shutdownGuest();
            if (vmachine.waitForGuestToShutDown(120000)) {
                addBuildLogEntry("Virtual machine '" + vmachine.getName() + "' guest OS shutdown within timeout of 2 minutes.");
                return success;
            } else {
                String errorMessage = "Virtual machine '" + vmachine.getName() + "' guest OS shutdown timed out.";
                addBuildLogEntry(errorMessage);
                throw new VMwareActionException(errorMessage);
            }
        }
    }

    private TaskInfoState powerOffVm(VMachine vmachine) {
        if (vmachine.isPoweredOff()) {
            addBuildLogEntry("Virtual machine '" + vmachine.getName() + "' has been already stopped");
            return success;
        } else if (vmachine.isSuspended()) {
            addBuildLogEntry("Can't stop virtual machine '" + vmachine.getName() + "' because it is suspended.");
            return success;
        } else {
            return vmachine.powerOff();
        }
    }

    private TaskInfoState suspendVm(VMachine vmachine) {
        if (vmachine.isPoweredOff()) {
            addBuildLogEntry("Can't suspend virtual machine '" + vmachine.getName() + "' because it is powered off.");
            return success;
        } else if (vmachine.isSuspended()) {
            addBuildLogEntry("Virtual machine '" + vmachine.getName() + "' has been already suspended.");
            return success;
        } else {
            return vmachine.suspend();
        }
    }

}
