package com.stiltsoft.bamboo.vmware.task;

import com.stiltsoft.bamboo.vmware.service.DefaultVMwareService;
import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.stiltsoft.bamboo.vmware.task.param.ActionParamHolder;
import com.vmware.vim25.TaskInfoState;

import static com.vmware.vim25.TaskInfoState.success;

public abstract class VMwareAction {

    public abstract TaskInfoState doAction(VMachine vmachine, ActionParamHolder paramHolder)
            throws VMwareActionException;

    public boolean execute(ActionParamHolder paramHolder) throws VMwareActionException {

        try {
            VMachine vmachine = new DefaultVMwareService(
                    paramHolder.getHost(),
                    paramHolder.getUserName(),
                    paramHolder.getPassword()).getVirtualMachine(paramHolder.getVmName());

            if (vmachine == null) {
                throw new VMwareActionException("No machine with name '" + paramHolder.getVmName() + "' exists!");
            } else {
                TaskInfoState result = doAction(vmachine, paramHolder);
                if (!result.equals(success)) {
                    throw new VMwareActionException("Unable to perform action, the result is: " + result);
                }
            }
        } catch (Exception e) {
            throw new VMwareActionException(e);
        }

        return true;
    }

}
