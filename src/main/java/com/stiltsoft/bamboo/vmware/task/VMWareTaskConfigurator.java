package com.stiltsoft.bamboo.vmware.task;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.stiltsoft.bamboo.vmware.task.param.PostBuildActionOption;
import com.stiltsoft.bamboo.vmware.task.param.StartVmStateOption;
import com.stiltsoft.bamboo.vmware.task.param.StopTaskActionOption;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static org.apache.commons.lang.StringUtils.*;

/**
 * Should be executable on an agent side too.
 */
public class VMWareTaskConfigurator extends AbstractTaskConfigurator {

    public static final String HOST = "vmware.host";
    public static final String LOGIN = "vmware.login";
    public static final String PASSWORD = "vmware.password";
    public static final String VM = "vmware.vm";
    public static final String VM_SNAPSHOT_NAME = "vmware.vm.snapshot.name";
    public static final String POST_BUILD_COMPLETE_ACTION = "vmware.post.build.complete.action";
    public static final String STOP_TASK_ACTION = "vmware.stop.task.action";
    public static final String START_VM_STATE = "vmware.start.vm.state";
    public static final String GUEST_START_TIMEOUT_SECONDS = "vmware.guest.start.timeout.sec";
    public static final String GUEST_START_TIMEOUT_ENABLED = "enableGuestStartTimeout";


    private static final String POST_BUILD_OPTIONS = "postBuildOptions";
    private static final String STOP_TASK_ACTION_OPTIONS = "stopTaskActionOptions";
    private static final String START_VM_STATE_OPTIONS = "startVmStateOptions";

    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params, @Nullable TaskDefinition previousTaskDefinition) {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(HOST, params.getString(HOST));
        config.put(LOGIN, params.getString(LOGIN));
        config.put(PASSWORD, params.getString(PASSWORD));
        config.put(VM, params.getString(VM));
        config.put(VM_SNAPSHOT_NAME, params.getString(VM_SNAPSHOT_NAME));
        config.put(POST_BUILD_COMPLETE_ACTION, params.getString(POST_BUILD_COMPLETE_ACTION));
        config.put(STOP_TASK_ACTION, params.getString(STOP_TASK_ACTION));
        config.put(START_VM_STATE, params.getString(START_VM_STATE));
        config.put(GUEST_START_TIMEOUT_SECONDS, params.getString(GUEST_START_TIMEOUT_SECONDS));
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        context.put(POST_BUILD_OPTIONS, PostBuildActionOption.values());
        context.put(STOP_TASK_ACTION_OPTIONS, StopTaskActionOption.values());
        context.put(START_VM_STATE_OPTIONS, StartVmStateOption.values());
        context.put(STOP_TASK_ACTION, StopTaskActionOption.STOP_VM);
        context.put(START_VM_STATE, StartVmStateOption.CURRENT);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        doPopulateContext(context, taskDefinition);
    }

    @Override
    public void populateContextForView(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForView(context, taskDefinition);
        doPopulateContext(context, taskDefinition);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);

        String hostValue = params.getString(HOST);
        String vmName = params.getString(VM);
        String startVmStateValue = params.getString(START_VM_STATE);
        String vmSnapshotName = params.getString(VM_SNAPSHOT_NAME);
        boolean guestTimeoutEnabled = params.getBoolean(GUEST_START_TIMEOUT_ENABLED);
        String guestStartTimeoutSecStr = params.getString(GUEST_START_TIMEOUT_SECONDS);

        if (isEmpty(hostValue)) {
            errorCollection.addError(HOST, "You must specify host URL");
        }
        if (isEmpty(vmName)) {
            errorCollection.addError(VM, "You must specify virtual machine name");
        }
        //START_VM_STATE absent in stop-task
        if (!isEmpty(startVmStateValue)) {
            if (StartVmStateOption.valueOf(startVmStateValue) == StartVmStateOption.SNAPSHOT) {
                if (isEmpty(vmSnapshotName)) {
                    errorCollection.addError(VM_SNAPSHOT_NAME,
                            "You must specify virtual machine snapshot name or use 'Start from current state' option");
                }
            }
        }

        if (guestTimeoutEnabled) {
            if (isBlank(guestStartTimeoutSecStr) || !isNumeric(guestStartTimeoutSecStr)) {
                errorCollection.addError(GUEST_START_TIMEOUT_SECONDS,
                        getI18nBean().getText("vmware.guest.start.timeout.sec.error"));
            }
        }
    }

    private void doPopulateContext(Map<String, Object> context, TaskDefinition taskDefinition) {
        context.put(HOST, taskDefinition.getConfiguration().get(HOST));
        context.put(LOGIN, taskDefinition.getConfiguration().get(LOGIN));
        context.put(PASSWORD, taskDefinition.getConfiguration().get(PASSWORD));
        context.put(VM, taskDefinition.getConfiguration().get(VM));
        context.put(VM_SNAPSHOT_NAME, taskDefinition.getConfiguration().get(VM_SNAPSHOT_NAME));
        context.put(POST_BUILD_COMPLETE_ACTION, taskDefinition.getConfiguration().get(POST_BUILD_COMPLETE_ACTION));
        context.put(POST_BUILD_OPTIONS, PostBuildActionOption.values());
        context.put(STOP_TASK_ACTION_OPTIONS, StopTaskActionOption.values());
        context.put(START_VM_STATE_OPTIONS, StartVmStateOption.values());

        String guestStartTimeoutStr = taskDefinition.getConfiguration().get(GUEST_START_TIMEOUT_SECONDS);
        context.put(GUEST_START_TIMEOUT_SECONDS, guestStartTimeoutStr);

        if (isNotBlank(guestStartTimeoutStr)) {
            context.put(GUEST_START_TIMEOUT_ENABLED, true);
        }

        String startVmState = taskDefinition.getConfiguration().get(START_VM_STATE);
        context.put(START_VM_STATE, isEmpty(startVmState) ? StartVmStateOption.CURRENT : startVmState);
        String stopTaskAction = taskDefinition.getConfiguration().get(STOP_TASK_ACTION);
        context.put(STOP_TASK_ACTION, isEmpty(stopTaskAction) ? StopTaskActionOption.STOP_VM : stopTaskAction);
    }

}
