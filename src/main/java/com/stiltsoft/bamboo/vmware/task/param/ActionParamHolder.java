package com.stiltsoft.bamboo.vmware.task.param;

import com.google.common.base.Optional;
import org.jetbrains.annotations.Nullable;

import java.util.Map;

import static com.google.common.base.Optional.absent;
import static com.stiltsoft.bamboo.vmware.task.VMWareTaskConfigurator.*;
import static com.stiltsoft.bamboo.vmware.task.param.PostBuildActionOption.DO_NOTHING;
import static com.stiltsoft.bamboo.vmware.task.param.StopTaskActionOption.SHUT_DOWN_GUEST;
import static java.lang.Integer.parseInt;
import static org.apache.commons.lang.StringUtils.*;
import static org.apache.commons.lang.time.DateUtils.MILLIS_PER_SECOND;

public class ActionParamHolder {

    private String host;

    private String userName;

    private String password;

    private String vmName;

    private String snapshotName;

    private String postBuildCompleteActionCode;

    private String stopTaskActionCode;

    private String startVmStateCode;

    private String guestStartTimeoutSecondsStr;

    public ActionParamHolder(Map<String, String> actionConfig) {
        host = actionConfig.get(HOST);
        userName = actionConfig.get(LOGIN);
        password = actionConfig.get(PASSWORD);
        vmName = actionConfig.get(VM);
        snapshotName = actionConfig.get(VM_SNAPSHOT_NAME);
        postBuildCompleteActionCode = actionConfig.get(POST_BUILD_COMPLETE_ACTION);
        stopTaskActionCode = actionConfig.get(STOP_TASK_ACTION);
        startVmStateCode = actionConfig.get(START_VM_STATE);
        guestStartTimeoutSecondsStr = actionConfig.get(GUEST_START_TIMEOUT_SECONDS);
    }

    public String getHost() {
        return host;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getVmName() {
        return vmName;
    }

    public String getSnapshotName() {
        return snapshotName;
    }

    public PostBuildActionOption getPostBuildCompleteOption() {
        return isEmpty(postBuildCompleteActionCode) ?
                DO_NOTHING : PostBuildActionOption.valueOf(postBuildCompleteActionCode);
    }

    public StopTaskActionOption getStopTaskActionOption() {
        return isEmpty(stopTaskActionCode) ? SHUT_DOWN_GUEST : StopTaskActionOption.valueOf(stopTaskActionCode);
    }

    @Nullable
    public StartVmStateOption getStartVmStateOption() {
        return isEmpty(startVmStateCode) ? null : StartVmStateOption.valueOf(startVmStateCode);
    }

    public Optional<Long> getGuestStartTimeoutMs() {
        if (isNotBlank(guestStartTimeoutSecondsStr) && isNumeric(guestStartTimeoutSecondsStr)) {
            int timeoutSeconds = parseInt(guestStartTimeoutSecondsStr);
            long timeoutMs = timeoutSeconds * MILLIS_PER_SECOND;
            return Optional.of(timeoutMs);
        } else {
            return absent();
        }
    }

    public String getDescription() {
        return this.toString();
    }

    @Override
    public String toString() {
        return "VMWare Task Parameters {" +
                "host='" + host + '\'' +
                ", userName='" + userName + '\'' +
                ", vmName='" + vmName + '\'' +
                ", snapshotName='" + snapshotName + '\'' +
                ", postBuildCompleteActionCode='" + postBuildCompleteActionCode + '\'' +
                ", stopTaskActionCode='" + stopTaskActionCode + '\'' +
                ", startVmStateCode='" + startVmStateCode + '\'' +
                ", guestStartTimeoutSecondsStr='" + guestStartTimeoutSecondsStr + '\'' +
                '}';
    }

}
