package com.stiltsoft.bamboo.vmware.task;

public class VMwareActionException extends Exception {

    public VMwareActionException(String message) {
        super(message);
    }

    public VMwareActionException(String message, Throwable cause) {
        super(message, cause);
    }

    public VMwareActionException(Throwable cause) {
        super(cause);
    }

}
