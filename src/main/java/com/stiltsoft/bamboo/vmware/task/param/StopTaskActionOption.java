package com.stiltsoft.bamboo.vmware.task.param;

public enum StopTaskActionOption {

    STOP_VM, // Power off (hard)
    SHUT_DOWN_GUEST, // Soft shutdown
    SUSPEND_VM

}
