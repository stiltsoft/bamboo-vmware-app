package com.stiltsoft.bamboo.vmware.task.param;

public enum PostBuildActionOption {

    DO_NOTHING,
    STOP_VM, // Power off (hard)
    SHUT_DOWN_GUEST, // Soft shutdown
    SUSPEND_VM

}
