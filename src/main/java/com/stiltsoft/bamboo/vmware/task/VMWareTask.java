package com.stiltsoft.bamboo.vmware.task;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.*;
import com.stiltsoft.bamboo.vmware.task.param.ActionParamHolder;
import org.jetbrains.annotations.NotNull;

public abstract class VMWareTask extends VMwareAction implements CommonTaskType {

    private BuildLogger buildLogger;

    @Override
    public TaskResult execute(@NotNull final CommonTaskContext taskContext) throws TaskException {
        buildLogger = taskContext.getBuildLogger();

        try {
            ActionParamHolder paramHolder = new ActionParamHolder(taskContext.getConfigurationMap());
            addBuildLogEntry(paramHolder.getDescription());

            return execute(paramHolder) ?
                    TaskResultBuilder.newBuilder(taskContext).success().build() :
                    TaskResultBuilder.newBuilder(taskContext).failed().build();
        } catch (VMwareActionException e) {
            buildLogger.addErrorLogEntry("Failed to execute VMware task.", e);
            return TaskResultBuilder.newBuilder(taskContext).failed().build();
        }
    }

    protected void addBuildLogEntry(String text) {
        buildLogger.addBuildLogEntry(text);
    }

}
