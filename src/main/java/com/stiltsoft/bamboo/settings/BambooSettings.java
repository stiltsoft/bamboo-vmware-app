package com.stiltsoft.bamboo.settings;

import static java.lang.Integer.getInteger;

public class BambooSettings {

    /**
     * https://confluence.atlassian.com/display/BAMBOO/Changing+the+remote+agent+heartbeat+interval
     */
    private static final String HEARTBEAT_TIMEOUT_SECONDS_PROP_NAME = "bamboo.agent.heartbeatTimeoutSeconds";

    private static final int heartbeatTimeoutSeconds = getInteger(HEARTBEAT_TIMEOUT_SECONDS_PROP_NAME, 600);

    public static int getHeartbeatTimeoutSeconds() {
        return heartbeatTimeoutSeconds;
    }

}