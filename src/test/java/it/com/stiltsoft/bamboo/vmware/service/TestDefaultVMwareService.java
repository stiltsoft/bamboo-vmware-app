package it.com.stiltsoft.bamboo.vmware.service;

import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.stiltsoft.bamboo.vmware.service.VMachineInfo;
import com.vmware.vim25.TaskInfoState;
import com.vmware.vim25.mo.VirtualMachineSnapshot;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Ignore
public class TestDefaultVMwareService extends AbstractVMwareServiceTest {

    public TestDefaultVMwareService(String url,
                                    String username,
                                    String password, String platformName, String platformVersion)
            throws MalformedURLException, RemoteException {
        super(url, username, password, platformName, platformVersion);
    }

    @Test
    public void connectionTest() throws Exception {
        Assert.assertEquals("Incorrect platform name", platformName, vmClient.getPlatformName());
        Assert.assertEquals("Incorrect platform version", platformVersion, vmClient.getPlatformVersion());
    }

    @Test
    public void getMachineListTest() throws Exception {
        List<VMachineInfo> infoList = vmClient.getVMachineInfoList();
        for (VMachineInfo vmInfo : infoList) {
            assertVm(vmClient.getVirtualMachine(vmInfo.getName()));
        }
    }

    @Test
    public void getMachineByNameTest() throws Exception {
        assertTrue(!vm1.getName().equals(vm2.getName()));
        assertVm(vm1);
        assertVm(vm2);
    }

    @Test
    public void checkedPowerOnOffVMTest() throws Exception {
        powerOnVM(vm1);
        powerOffVM(vm1);
    }

    @Test
    public void uncheckedPowerOnOffVMTest() throws Exception {
        assertEquals(vm1.powerOff(), TaskInfoState.error);
        powerOnVM(vm1);
        assertEquals(vm1.powerOn(), TaskInfoState.error);
    }

    @Test
    public void checkedSuspendVMTest() throws Exception {
        powerOnVM(vm1);
        suspendVM(vm1);
        powerOnVM(vm1);
    }

    @Test
    public void uncheckedSuspendVMTest() {
        assertEquals(vm1.suspend(), TaskInfoState.error);
        powerOnVM(vm1);
        suspendVM(vm1);
        Assert.assertEquals(vm1.suspend(), TaskInfoState.error);
    }

    @Test
    public void powerOffAfterSuspendVMTest() {
        powerOnVM(vm1);
        suspendVM(vm1);
        Assert.assertEquals(vm1.powerOff(), TaskInfoState.error);
    }

    @Test
    public void getVMSnapshotListTest() {
        List<String> expectedSnapshotNames = new ArrayList<String>();
        Collections.addAll(expectedSnapshotNames, "Snapshot 1(4mb-on)", "Snapshot 2(8mb-off)",
                "Snapshot 3(16mb-on)", "Snapshot 4(128mb-off)");
        Assert.assertEquals(expectedSnapshotNames, vm3snap.getSnapshotNames());
    }

    @Test
    public void revertVmToSnapshotTest() {
        for (SnapState snapState : SnapState.values()) {
            System.out.println("Snapshot: " + snapState.getSnapName());
            revertVmToSnapshot(vm3snap, snapState.getSnapName());
            assertSnapshotReverted(vm3snap, snapState);
        }
    }

    @Test
    public void getSnapshotWhenNoSnapshotsAtAll() {
        Assert.assertEquals(0, vm1.getSnapshotNames().size());
        Assert.assertNull(vm1.getSnapshot("Snapshot 1"));
    }

    @Test
    public void getSnapshotIfItIsNot() {
        Assert.assertTrue(vm3snap.getSnapshotNames().size() > 0);
        Assert.assertNull(vm3snap.getSnapshot("Snapshot NONEXISTENCE"));
    }

    @Test
    public void testSnapshotListRecreated() {
        Assert.assertEquals(vm3snap.getSnapshotNames(), vm3snap.getSnapshotNames());
    }

    private void powerOnVM(VMachine vm) {
        assertEquals(TaskInfoState.success, vm.powerOn());
        assertTrue("VM isn't powered on", vm.isPoweredOn());
    }

    private void powerOffVM(VMachine vm) {
        assertEquals(TaskInfoState.success, vm.powerOff());
        assertTrue("VM isn't powered off", vm.isPoweredOff());
    }

    private void suspendVM(VMachine vm) {
        assertEquals(TaskInfoState.success, vm.suspend());
        assertTrue("VM isn't suspended", vm.isSuspended());
    }

    private void revertVmToSnapshot(VMachine vm, String snapshotName) {
        VirtualMachineSnapshot snap = vm.getSnapshot(snapshotName);
        Assert.assertNotNull(snap);
        assertEquals(TaskInfoState.success, vm.revertToSnapshot(snap));
    }

}