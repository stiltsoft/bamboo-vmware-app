package it.com.stiltsoft.bamboo.vmware.service;

import com.stiltsoft.bamboo.vmware.service.DefaultVMwareService;
import com.stiltsoft.bamboo.vmware.service.VMachine;
import com.vmware.vim25.TaskInfoState;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

@RunWith(value = Parameterized.class)
public abstract class AbstractVMwareServiceTest {

    protected static final String TEST_VM_1 = "ubuntu32";
    protected static final String TEST_VM_2 = "test vm2";
    protected static final String TEST_VM_3_SNAP = "test vm3 snap";

    private String url;
    private String username;
    private String password;

    protected String platformName;
    protected String platformVersion;

    protected DefaultVMwareService vmClient;

    protected VMachine vm1;
    protected VMachine vm2;
    protected VMachine vm3snap;


    public AbstractVMwareServiceTest(String url,
                                     String username,
                                     String password, String platformName, String platformVersion)
            throws MalformedURLException, RemoteException {

        this.url = url;
        this.username = username;
        this.password = password;
        this.platformName = platformName;
        this.platformVersion = platformVersion;
    }

    @Parameterized.Parameters
    public static Collection<String[]> data() throws ConfigurationException {
        return getTestParams();
    }

    @Before
    public void beforeTest() throws MalformedURLException, RemoteException, InterruptedException {
        System.out.println("Parameters: " + this);
        vmClient = new DefaultVMwareService(url, username, password);
        vm1 = vmClient.getVirtualMachine(TEST_VM_1);
//        vm2 = vmClient.getVirtualMachine(TEST_VM_2);
//        vm3snap = vmClient.getVirtualMachine(TEST_VM_3_SNAP);
//        resetMachineState();
    }

    @After
    public void afterTest() {
        vmClient.disconnect();
    }

    private void resetMachineState() throws RemoteException, InterruptedException {
        turnOffVM(vm1);
//        turnOffVM(vm2);
//        turnOffVM(vm3snap);
    }

    protected void turnOffVM(VMachine vm) throws InterruptedException, RemoteException {
        if (vm.isPoweredOn()) {
            if (!vm.powerOff().equals(TaskInfoState.success) || !vm.isPoweredOff()) {
                throw new RuntimeException("Can't stop VM: " + vm.getName());
            }
        } else if (vm.isSuspended()) {
            if (!vm.powerOn().equals(TaskInfoState.success) ||
                    !vm.powerOff().equals(TaskInfoState.success) ||
                    !vm.isPoweredOff()) {
                throw new RuntimeException("Can't stop VM: " + vm.getName());
            }
        }
    }

    protected static List<String[]> getTestParams() throws ConfigurationException {
        PropertiesConfiguration config = new PropertiesConfiguration(AbstractVMwareServiceTest.class.getResource("test.properties"));
        Iterator keys = config.getKeys("test.data");

        List<String[]> paramList = new ArrayList<String[]>();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            paramList.add(config.getStringArray(key));
        }
        return paramList;
    }

    protected void assertVm(VMachine vm) {
        if (vm.getName().equals(TEST_VM_1)) {
            Assert.assertEquals("Incorrect CPU count", 1, vm.getCpuCount());
            Assert.assertEquals("Incorrect memory size", 128, vm.getMemorySizeMB());
            return;
        }

        if (vm.getName().equals(TEST_VM_2)) {
            Assert.assertEquals("Incorrect CPU count", 2, vm.getCpuCount());
            Assert.assertEquals("Incorrect memory size", 32, vm.getMemorySizeMB());
            return;
        }

        if (vm.getName().equals(TEST_VM_3_SNAP)) {
            Assert.assertEquals("Incorrect CPU count", 1, vm.getCpuCount());
        } else {
            throw new RuntimeException("Unknown machine to check");
        }
    }

    @SuppressWarnings({"UnusedDeclaration"})
    protected enum SnapState {

        SNAP_1("Snapshot 1(4mb-on)", 4, true), SNAP_2("Snapshot 2(8mb-off)", 8, false),
        SNAP_3("Snapshot 3(16mb-on)", 16, true), SNAP_4("Snapshot 4(128mb-off)", 128, false);

        private String snapName;
        private int memorySizeMb;
        private boolean poweredOn;

        SnapState(String snapName, int memorySizeMb, boolean poweredOn) {
            this.snapName = snapName;
            this.memorySizeMb = memorySizeMb;
            this.poweredOn = poweredOn;
        }

        public String getSnapName() {
            return snapName;
        }

        public int getMemorySizeMb() {
            return memorySizeMb;
        }

        public boolean isPoweredOn() {
            return poweredOn;
        }
    }

    protected void assertSnapshotReverted(VMachine vm, SnapState snapState) {
        Assert.assertEquals(snapState.getMemorySizeMb(), vm.getMemorySizeMB());
        Assert.assertTrue(snapState.isPoweredOn() == vm3snap.isPoweredOn());
    }


    @Override
    public String toString() {
        return "{" +
                "url='" + url + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", platformName='" + platformName + '\'' +
                ", platformVersion='" + platformVersion + '\'' +
                '}';
    }

}