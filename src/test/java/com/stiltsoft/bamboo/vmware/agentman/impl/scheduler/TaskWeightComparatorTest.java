package com.stiltsoft.bamboo.vmware.agentman.impl.scheduler;

import com.stiltsoft.bamboo.vmware.agentman.api.scheduler.PeriodicTask;
import org.junit.Test;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.shuffle;
import static java.util.Collections.sort;
import static org.junit.Assert.assertEquals;

public class TaskWeightComparatorTest {

    @Test
    public void testCompare() throws Exception {
        TaskWeightComparator comparator = new TaskWeightComparator();

        PeriodicTask task10 = createTask(10);
        PeriodicTask task11 = createTask(11);
        PeriodicTask task20 = createTask(20);

        List<PeriodicTask> tasks = asList(task11, task20, task10);

        for (int i = 0; i < 5; i++) {
            sort(tasks, comparator);

            assertEquals(task10, tasks.get(0));
            assertEquals(task11, tasks.get(1));
            assertEquals(task20, tasks.get(2));

            shuffle(tasks);
        }
    }

    private PeriodicTask createTask(final int weight) {
        return new PeriodicTask() {
            @Override
            public void execute() {
            }

            @Override
            public int getWeight() {
                return weight;
            }
        };
    }

}
