package com.stiltsoft.bamboo.vmware.agentman.impl;

import com.atlassian.bamboo.buildqueue.manager.AgentManager;
import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AgentActivityCacheTest {

    @Test
    public void testUpdateAndGetActivated() throws Exception {
        AgentManager agentManager = mock(AgentManager.class);
        List<BuildAgent> agentsState1 = getBuildAgentList(true, true, false, true, false);
        List<BuildAgent> agentsState2 = getBuildAgentList(true, true, true, true, true);
        when(agentManager.getAllRemoteAgents())
                .thenReturn(agentsState1)
                .thenReturn(agentsState2);

        AgentActivityCache activityCache = new AgentActivityCache(agentManager);

        List<BuildAgent> activatedAgents = activityCache.updateAndGetActivated();
        assertEquals(2, activatedAgents.size());

        assertEquals(3, activatedAgents.get(0).getId());
        assertEquals(5, activatedAgents.get(1).getId());
    }

    @Test
    public void testGetBuildAgentList() throws Exception {
        List<BuildAgent> agents = getBuildAgentList(true, true, false, true);
        int activeAgentCount = 0;
        int inactiveAgentCount = 0;
        long idCounter = 0;
        for (BuildAgent agent : agents) {
            if (agent.isActive()) {
                activeAgentCount++;
            } else {
                inactiveAgentCount++;
            }
            idCounter += agent.getId();
        }

        assertEquals(10, idCounter);
        assertEquals(3, activeAgentCount);
        assertEquals(1, inactiveAgentCount);
    }

    private List<BuildAgent> getBuildAgentList(boolean... activities) {
        List<BuildAgent> agents = new ArrayList<BuildAgent>();
        long idCounter = 1;
        for (boolean activity : activities) {
            agents.add(createAgent(idCounter++, activity));
        }
        return agents;
    }

    private BuildAgent createAgent(long id, boolean isActive) {
        BuildAgent agent = mock(BuildAgent.class);
        when(agent.getId()).thenReturn(id);
        when(agent.isActive()).thenReturn(isActive);
        return agent;
    }

}
