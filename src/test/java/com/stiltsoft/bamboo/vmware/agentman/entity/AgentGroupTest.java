package com.stiltsoft.bamboo.vmware.agentman.entity;

import com.atlassian.bamboo.v2.build.agent.BuildAgent;
import org.junit.Assert;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;

import static java.util.Collections.sort;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class AgentGroupTest {

    @Test
    public void testCompareTo() throws Exception {
        List<AgentGroup> agentGroups = new LinkedList<AgentGroup>();
        agentGroups.add(createGroup(8));
        agentGroups.add(createGroup(10));
        agentGroups.add(createGroup(4));

        sort(agentGroups);

        assertEquals(10, agentGroups.get(0).size());
        assertEquals(8, agentGroups.get(1).size());
        assertEquals(4, agentGroups.get(2).size());
    }

    private AgentGroup createGroup(int size) {
        AgentGroup group = new AgentGroup();
        for (int i = 0; i < size; i++) {
            group.add(createAgent());
        }
        return group;
    }

    private BuildAgent createAgent() {
        return mock(BuildAgent.class);
    }

}
